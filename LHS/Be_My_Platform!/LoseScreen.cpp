/******************************************************************************/
/*!
\file   LoseScreen.cpp
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "LoseScreen.h"

const SDL_Color BLACK = { 0, 0, 0, 255 };

void LoseScreen::Initialize()
{
    // remove the standard mouse
    SDL_ShowCursor(false);

    // Set camera
    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0);
    
    // Set the gravity for mouse
    m_useBuiltInPhysics = false;
    b2Vec2 gravity(0.f, -9.8f);
    SetCustomPhysicsWorld(gravity);

    // Set and add the objects
    Set_Object(Lose_Screen, "LoseScreen", { 0.0f, 0.0f, 0.0f }, { State::m_width, State::m_height });
    Set_Object(button[0], "Button_LevelSelect", { -123.0f, -185.0f, 50.0f }, { 180.0f, 180.0f });
    Set_Object(button[1], "Button_Retry", { 172.0f, -180.f, 50.0f }, { 210.0f, 210.0f });
    Set_Object(button[2], "Button_Key", { 463.0f, -185.0f, 50.0f }, { 180.0f, 180.0f });

    // Mouse
    mouse.Mouse_Init(GetCustomPhysicsWorld());
    AddObject(&mouse);
}

void LoseScreen::Update(float dt)
{
    // Mouse update
    mouse.Mouse_Update(GetCustomPhysicsWorld());

    // check the collision between mouse and buttons
    for (int i = 0; i < 3; i++)
    {
        if (mouse.IsCollidingWith(&button[i]) && IsOn == false)
        {
            // change the color of button when the mouse hover on it
            button[i].sprite.color.a = 50;
            
            if (m_input->IsTriggered(SDL_BUTTON_LEFT))
            {   
                // first button is level_select button
                if (i == 0)
                    m_game->Change(Level_select);
                // second is retry
                else if(i == 1)
                    m_game->Change(LV_one);
                else if(i == 2)
                {
                    // Set the change_key window
                    Set_Object(change_key, "Change_Key", { 0.0f, 0.0f, 100.0f }, { 500.0f, 700.0f });
                    Set_Object(yes, "yes", { 5.0f, -162.f, 100.0f }, { 375.0f, 60.0f });
                    Set_Object(no, "no", { 5.0f, -258.0f, 100.0f }, { 375.0f, 60.0f });
                    IsOn = true;
                }
                    
            }
        }
        // if button collide with anything clear the color
        else
            button[i].sprite.color.a = 0;
    }

    // the change key window is shown
    if(IsOn == true)
    {
        if (m_input->IsTriggered(SDL_BUTTON_LEFT))
        {
            // if the player click yes
            if (mouse.IsCollidingWith(&yes))
            {
                m_game->Is_Control_Change = !m_game->Is_Control_Change;
                RemoveObject(&change_key);
                RemoveObject(&yes);
                RemoveObject(&no);
                IsOn = false;
            }
            // if the player click no
            if (mouse.IsCollidingWith(&no))
            {
                RemoveObject(&change_key);
                RemoveObject(&yes);
                RemoveObject(&no);
                IsOn = false;
            }
        }
    }

    UpdatePhysics(dt);
    Render(dt);
}

void LoseScreen::Close()
{
    // Wrap up state
    ClearBaseState();
}

void LoseScreen::Set_Object(Object& object, const char* name, b2Vec3 position, b2Vec2 scale)
{
    // Set name
    object.SetName(name);

    // Set transform
    object.transform.position = position;
    object.transform.SetScale(scale);

    // Set sprite
    object.sprite.color = { 0, 0, 0, 0 };

    // background
    if (object.GetName() == "LoseScreen")
    {
        if (Death_Kind == Kind_Of_Death::Eating_Chicken)
            object.sprite.LoadImage("texture/WinLose/Lose_Eat.png", m_renderer);
        else if (Death_Kind == Kind_Of_Death::Fire_Chicken)
            object.sprite.LoadImage("texture/WinLose/Lose_Chicken.png", m_renderer);
        else if (Death_Kind == Kind_Of_Death::Timed_Death)
            object.sprite.LoadImage("texture/WinLose/Lose_TimeOut.png", m_renderer);

        object.sprite.color = { 255, 255, 255, 255 };
    }
    // buttons
    else if (object.GetName() == "Button_LevelSelect" ||
        object.GetName() == "Button_Retry" ||
        object.GetName() == "Button_Key")
        object.sprite.LoadImage("texture/circle.png", m_renderer);
    else if (object.GetName() == "Change_Key")
    {
        object.sprite.LoadImage("texture/Button/Change_Key.png", m_renderer);
        object.sprite.color = { 255, 255, 255, 255 };
    }
    else if (object.GetName() == "yes" || object.GetName() == "no")
        object.sprite.LoadImage("texture/rect.png", m_renderer);

    AddObject(&object);
}
