/******************************************************************************/
/*!
\file   LoseScreen.h
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "engine\State.h"
#include "engine\Object.h"

class LoseScreen : public State
{
    friend class Game;

protected:

    LoseScreen() : State() {};
    ~LoseScreen() override {};

    void Initialize() override;
    void Update(float dt) override;
    void Close() override;

private:
    // to check how the chick die
    Kind_Of_Death Death_Kind = Kind_Of_Death::Eating_Chicken;

    const int period = 180;
    float ellapsedtime = 0.0f;

    // Background object
    Object Lose_Screen;
    // buttons
    Object button[3];
    Object change_key;
    Object yes, no;
    bool IsOn = false;

    Mouse mouse;

    // Set the values for object and add
    void Set_Object(Object& object, const char* name, b2Vec3 position, b2Vec2 scale);
    void Set_Change_Key();
};