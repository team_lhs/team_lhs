/******************************************************************************/
/*!
\file   Game.cpp
\par    Project Name : Be_My_Platform!
\author Minui Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Game.h"
#include "engine/StateManager.h"
#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/filewritestream.h"
#include "rapidjson/filereadstream.h"
#include <cstdio>
#include <iostream>

using namespace rapidjson;
bool Game::isFullScreen = false;
int Game::Volume = 128;
int Game::Dog_ver = 0;

Game::Game()
{
	m_stateManager = new StateManager;
}

Game::~Game()
{
	if (m_stateManager) {
		delete m_stateManager;
		m_stateManager = nullptr;
	}
}

bool Game::Initialize()
{
    State::m_width = SCREEN_WIDTH, State::m_height = SCREEN_HEIGHT;

    // Set state ids
    splash_screen.SetId(LV_splash);
    mainmenu.SetId(LV_main);

    level_one.SetId(LV_one);
    level_map_editor.SetId(LV_map_editor);

    win_screen.SetId(State_win);
    lose_screen.SetId(State_lose);
    option.SetId(State_Option);
    credit.SetId(State_Credit);

    level_select.SetId(Level_select);

    // Register states
    RegisterState(&splash_screen);
    RegisterState(&mainmenu);

    RegisterState(&level_one);
    RegisterState(&level_map_editor);

    RegisterState(&win_screen);
    RegisterState(&lose_screen);
    RegisterState(&option);
    RegisterState(&level_select);
    RegisterState(&credit);

    SetFirstState(&splash_screen);

    GetCurrentState()->Initialize();

    click_sound.sound.LoadSound("sound/click.wav");
    backgroundmusic.sound.LoadSound("sound/main.wav");
    levelchanged = false;
    return true;
}

void Game::Update(float dt)
{
    StateManager::SetWindowSize(m_width, m_height);
    m_stateManager->Update(dt);

    click_sound.sound.SetVolume(Volume);

    if (m_stateManager->GetCurrentState()->GetId() == LV_main ||
        m_stateManager->GetCurrentState()->GetId() == Level_select ||
        m_stateManager->GetCurrentState()->GetId() == State_Option ||
        m_stateManager->GetCurrentState()->GetId() == State_Credit ||
        level_one.IsPause != false)
    if (State::m_input->IsTriggered(SDL_BUTTON_LEFT))
        click_sound.sound.Play();

    if (GetCurrentState()->GetId() == 1 && !levelchanged)
    {
        backgroundmusic.sound.Play(-1);
        levelchanged = true;
    }

    if (previousid != GetCurrentState()->GetId())
    {
        if ((previousid == 2 || previousid == 4 || previousid == 5) &&
            (GetCurrentState()->GetId() == 9))
        {
            backgroundmusic.sound.Free();
            backgroundmusic.sound.LoadSound("sound/main.wav");
            backgroundmusic.sound.Play(-1);
        }

        if (GetCurrentState()->GetId() == 2)
        {
            backgroundmusic.sound.Free();
            backgroundmusic.sound.LoadSound("sound/background.mp3");
            backgroundmusic.sound.Play(-1);
        }
        else if (GetCurrentState()->GetId() == 4)
        {
            backgroundmusic.sound.Free();
            backgroundmusic.sound.LoadSound("sound/lose.wav");
            backgroundmusic.sound.Play(-1);
        }
        else if (GetCurrentState()->GetId() == 5)
        {
            backgroundmusic.sound.Free();
            backgroundmusic.sound.LoadSound("sound/win.wav");
            backgroundmusic.sound.Play(-1);
        }
        previousid = GetCurrentState()->GetId();

    }
    else if(GetCurrentState()->GetId() == 3)
    {
        backgroundmusic.sound.Free();
        previousid = GetCurrentState()->GetId();
        return;
    }
    backgroundmusic.sound.SetVolume(Volume);
}

void Game::Close()
{
	m_stateManager->Close();
}

void Game::RegisterPauseState(State* state)
{
	m_stateManager->RegisterPauseState(state);
}

void Game::Pause()
{
	m_stateManager->Pause();
}

void Game::Resume()
{
	m_stateManager->Resume();
}

void Game::Restart()
{
	m_stateManager->Restart();
}

void Game::RegisterState(State* state)
{
	m_stateManager->RegisterState(state);
}

void Game::SetFirstState(State* state)
{
	m_stateManager->SetFirstState(state);
}

State* Game::GetCurrentState()
{
	return m_stateManager->GetCurrentState();
}

int Game::GetCurrentMapStar()
{
    return Level_Star[level_one.Current_Map_Number];
}

void Game::Quit()
{
	m_stateManager->Quit();
}

void Game::Change(unsigned stateId)
{
    m_stateManager->Change(stateId);
}

void Game::Change(unsigned stateId, Kind_Of_Death Kind_Death)
{
    m_stateManager->Change(stateId);
    lose_screen.Death_Kind = Kind_Death;
}

void Game::Change(unsigned stateId,int map_number)
{
    m_stateManager->Change(stateId);
    if (stateId == LV_map_editor || stateId == LV_one)
    {
        current_map = map_number;

        level_one.Current_Map_Number = map_number;
        level_map_editor.Current_Map_Number = map_number;
    }
}

void Game::Save()
{
	errno_t err;
	FILE* fp;
	err = fopen_s(&fp, "level.json", "wb"); // non-Windows use "r"

	char writeBuffer[65536];
	FileWriteStream os(fp, writeBuffer, sizeof(writeBuffer));
	PrettyWriter<FileWriteStream> writer(os);
	writer.StartObject();
	writer.Key("Total Level");
	writer.Int(static_cast<int>(Level_Star.size()));
	writer.Key("MapInfo");
	writer.StartArray();
	for (auto i = 0; i<=static_cast<int>(Level_Star.size()-1); ++i)
	{
		writer.StartObject();
		writer.Key("Star");
		writer.Int(Level_Star[i]);

		writer.EndObject();
	}
	writer.EndArray();
	writer.EndObject();
	fclose(fp);
}
 
bool Game::IsQuit()
{
	return m_stateManager->IsQuit();
}

SDL_Color& operator+=(SDL_Color& a, SDL_Color b)
{
    SDL_Color result = a;
    a.r += b.r;
    a.g += b.g;
    a.b += b.b;
    a.a += b.a;
    return a;
}

SDL_Color& operator-=(SDL_Color& a, SDL_Color b)
{
    SDL_Color result = a;
    a.r -= b.r;
    a.g -= b.g;
    a.b -= b.b;
    a.a -= b.a;
    return a;
}
