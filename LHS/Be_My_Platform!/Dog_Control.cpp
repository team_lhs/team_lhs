/******************************************************************************/
/*!
\file   Dog_Control.cpp
\par    Project Name : Be_My_Platform!
\author SeongWook Shin

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "Dog.h"
#include "Dog_Control.h"

const SDL_Color GREY = { 136, 136, 136, 255 };
const SDL_Color BLACK = { 0, 0, 0, 255 };
const SDL_Color CORAL_RED = { 255, 68, 68, 255 };
const SDL_Color WHITE = { 255, 255, 255, 255 };

const std::string Dog_head_path = "texture/Character/Dog_Head_ver";
const std::string Dog_head_ani_path = "texture/Character/Dog_Head_Ani_ver";
const std::string Dog_head_ani_flip_path = "texture/Character/Dog_Head_Ani_Flip_ver";

Dog_Control::Dog_Control(SDL_Renderer* m_renderer,b2World* custom_Physics_world ,float x, float y, float scale, int dogver)
{
    dog_ver = dogver;
    Current_world = custom_Physics_world;
    Head_Heading = b2Vec2(1.f, 0.f);
    Dog_Scale = scale;
    //For making tail
    dog.push_back(new Dog);
    dog[0]->SetName("dog");
    dog[0]->Direction = { 1.f,0.f };
    dog[0]->transform.position.Set(x, y, 0.0f);
    dog[0]->transform.SetScale(scale, scale);
    dog[0]->transform.rotation = -90;
    dog[0]->sprite.color = WHITE;
    dog[0]->sprite.LoadImage("texture/Character/Dog_Tail.png", m_renderer);
    dog[0]->customPhysics.pOwnerTransform = &dog[0]->transform;
    dog[0]->customPhysics.bodyType = CustomPhysics::STATIC;
    dog[0]->customPhysics.bodyShape = CustomPhysics::BOX;
    dog[0]->customPhysics.AllocateBody(custom_Physics_world);

    //For making body
    dog.push_back(new Dog);
    dog[1]->SetName("dog_body");
    dog[1]->Direction = { 1.f,0.f };
    dog[1]->transform.position.Set(x+scale*1.5f-1.f, y, 0.0f);
    dog[1]->transform.SetScale(scale*2, scale);
    dog[1]->transform.rotation = 0;
    dog[1]->sprite.color = WHITE;
    dog[1]->sprite.LoadImage("texture/Character/Dog_Body.png", m_renderer);
    dog[1]->customPhysics.pOwnerTransform = &dog[1]->transform;
    dog[1]->customPhysics.bodyType = CustomPhysics::STATIC;
    dog[1]->customPhysics.bodyShape = CustomPhysics::BOX;
    dog[1]->customPhysics.AllocateBody(custom_Physics_world);

    //For making head
    dog.push_back(new Dog);
    dog[2]->SetName("dog");
    dog[2]->Direction = { 1.f,0.f };
    dog[2]->transform.position.Set(x + scale*3-2.f, y, 0.0f);
    dog[2]->transform.SetScale(scale, scale);
    dog[2]->transform.rotation = -90;
    dog[2]->sprite.color = WHITE;
    std::string temp = Dog_head_path + std::to_string(dog_ver)+ ".png";
    dog[2]->sprite.LoadImage(temp.c_str(), m_renderer);
    dog[2]->customPhysics.pOwnerTransform = &dog[2]->transform;
    dog[2]->customPhysics.bodyType = CustomPhysics::STATIC;
    dog[2]->customPhysics.bodyShape = CustomPhysics::BOX;
    dog[2]->customPhysics.AllocateBody(custom_Physics_world);

    Dog_Head_Ghost.SetName("dog_ghost");
    Dog_Head_Ghost.transform.position = dog[2]->transform.position;
    Dog_Head_Ghost.transform.SetScale(scale - 28.f, scale -18.f);
    Dog_Head_Ghost.transform.rotation = dog[2]->transform.rotation;
    Dog_Head_Ghost.sprite.color = SDL_Color{ 255,0,255,0 };
    Dog_Head_Ghost.sprite.LoadImage("texture/rect.png", m_renderer);
    Dog_Head_Ghost.customPhysics.pOwnerTransform = &Dog_Head_Ghost.transform;
    Dog_Head_Ghost.customPhysics.bodyType = CustomPhysics::STATIC;
    Dog_Head_Ghost.customPhysics.bodyShape = CustomPhysics::BOX;
    //Dog_Head_Ghost.customPhysics.ActiveGhostCollision(true);
    Dog_Head_Ghost.customPhysics.AllocateBody(custom_Physics_world, _EntityCategory::Dog_Ghost,_EntityCategory::Dog_Ghost | Enemy | Interaction_Object);
}

void Dog_Control::Update_Dog(SDL_Renderer* m_renderer,float dt)
{
    Make_New_Tail=Update_Tail(m_renderer, dt);
    Make_New_Head=Update_Head(m_renderer, dt);


    return;
}

bool Dog_Control::Update_Head(SDL_Renderer * m_renderer, float dt)
{
    const b2Vec3 pos_val1 = { Head_Heading.x / 2.f, Head_Heading.y / 2.f,0.f };
    const b2Vec3 pos_val2 = { Head_Heading.x , Head_Heading.y,0.f };
    const b2Vec2 scale_val1 = { abs(Head_Heading.x) ,abs(Head_Heading.y) };

    Dog_Head_Ghost.transform.position = dog[dog.size() - 1]->transform.position+10*pos_val2;
    Dog_Head_Ghost.transform.rotation = dog[dog.size() - 1]->transform.rotation;
    Dog_Head_Ghost.sprite.color = { 255,0,0,0 };
    //Dog_Head_Ghost.customPhysics.ActiveGhostCollision(true);
    Dog_Head_Ghost.customPhysics.AllocateBody(Current_world);

    if (dog[dog.size() - 1]->Is_Animated)             //head animation check
    {
        dog[dog.size() - 1]->Elapsed_Time += dt;
        if (dog[dog.size() - 1]->Elapsed_Time > 0.33f)
        {
            Make_Head(m_renderer);                                   //replace head to curve and make head and body!
            dog[dog.size() - 3]->Elapsed_Time = 0;
            return true;
        }
        return false;
    }

    //For extend dog[end-1](body)  and move dog[end](head) along with dog[end-1](body)
    auto i = dog.end() - 2;
    
    const auto Original_Scale = (*i)->transform.GetScale();
    (*i)->transform.SetScale( Original_Scale+ scale_val1);

    if (i == dog.begin() + 1 && dog[0]->Is_Animated == false)
        (*i)->transform.position += pos_val2;
    else if (i == dog.begin() + 1 && dog[0]->Is_Animated == true)
    {
        (*i)->transform.position += pos_val1;
    }
    else
        (*i)->transform.position += pos_val1;

    (*i)->customPhysics.AllocateBody(Current_world);

    i = dog.end() - 1;
    (*i)->transform.position += pos_val2;

    (*i)->customPhysics.AllocateBody(Current_world);
    return false;
}

bool Dog_Control::Update_Tail(SDL_Renderer * m_renderer, float dt)
{
    if (dog[0]->Is_Animated)             //tail animation check
    {
        dog[0]->Elapsed_Time += dt;
        if (dog[0]->Elapsed_Time > 0.33f)
        {
            int angle = (int)dog[0]->Next_Rotation;

            if (dog[0]->Rotate_Image_Direction == Left)
            {
                switch (angle)
                {
                case 0:
                    dog[0]->transform.position -= b2Vec3(-Dog_Scale*0.375f, -Dog_Scale *0.125f, 0);
                    break;
                case 90:
                    dog[0]->transform.position -= b2Vec3(Dog_Scale*0.125f, -Dog_Scale *0.375f, 0);
                    break;
                case 180:
                    dog[0]->transform.position -= b2Vec3(Dog_Scale*0.375f, Dog_Scale*0.15f, 0);
                    break;
                case 270:
                    dog[0]->transform.position -= b2Vec3(-Dog_Scale *0.15f, Dog_Scale*0.375f, 0);
                    break;
                }
            }
            else if (dog[0]->Rotate_Image_Direction == Right)
            {
                switch (angle)
                {
                case 0:
                    dog[0]->transform.position -= b2Vec3(Dog_Scale*0.375f, -Dog_Scale *0.125f, 0);
                    break;
                case 90:
                    dog[0]->transform.position -= b2Vec3(Dog_Scale*0.125f, Dog_Scale*0.375f, 0);
                    break;
                case 180:
                    dog[0]->transform.position -= b2Vec3(-Dog_Scale *0.375f, Dog_Scale*0.15f, 0);
                    break;
                case 270:
                    dog[0]->transform.position -= b2Vec3(-Dog_Scale *0.15f, -Dog_Scale *0.375f, 0);
                    break;
                }
            }
            const float Original_Scale_x = dog[0]->transform.GetScale().x;
            const float Original_Scale_y = dog[0]->transform.GetScale().y;
            dog[0]->transform.SetScale(Original_Scale_x*4.f / 7.f, Original_Scale_y*4.f / 5.f);
            dog[0]->transform.rotation = dog[0]->Next_Rotation;
            dog[0]->sprite.Free();
            dog[0]->sprite.LoadImage("texture/Character/Dog_Tail.png", m_renderer);
            dog[0]->sprite.activeAnimation = false;
            dog[0]->Is_Animated = false;
            dog[0]->customPhysics.AllocateBody(Current_world);
            return false;                           //delete tail and replace it
        }
        return false;
    }

	if (Extend_Length > 0)
	{
		for (int i = 0; i < Extend_Length; ++i)
		{
			Extend_Dog(Current_world);
		}
		Extend_Length = 0;
	}

    auto i = dog.begin() + 1;

    //For eraisng last tail and body since body scale<0 
    //erase dog[0], and dog[1] change next dog[0] to tail and next dog[1] to body
    if ((*i)->transform.GetScale().x<0 || (*i)->transform.GetScale().y < 0)
    {
        int angle = (int)dog[2]->Next_Rotation;

        if (dog[2]->Rotate_Image_Direction == Left)
        {
            dog[2]->sprite.Free();
            dog[2]->sprite.LoadImage("texture/Character/Dog_Tail_Ani.png", m_renderer);
            switch (angle)
            {
            case 0:
                dog[2]->transform.position += b2Vec3(-Dog_Scale *0.375f, -Dog_Scale *0.125f, 0);
                break;
            case 90:               
                dog[2]->transform.position += b2Vec3(Dog_Scale*0.125f,-Dog_Scale *0.375f, 0);
                break;
            case 180:
                dog[2]->transform.position += b2Vec3(Dog_Scale*0.375f, Dog_Scale*0.125f, 0);
                break;
            case 270:
                dog[2]->transform.position += b2Vec3(-Dog_Scale *0.125f, Dog_Scale*0.375f, 0);
                break;
            }
        }
        else if(dog[2]->Rotate_Image_Direction == Right)
        {
            dog[2]->sprite.Free();
            dog[2]->sprite.LoadImage("texture/Character/Dog_Tail_Ani_Flip.png", m_renderer);
            switch (angle)
            {
            case 0:
                dog[2]->transform.position += b2Vec3(Dog_Scale*0.375f, -Dog_Scale *0.125f, 0);
                break;
            case 90:
                dog[2]->transform.position += b2Vec3(Dog_Scale*0.125f, Dog_Scale*0.375f, 0);
                break;
            case 180:
                dog[2]->transform.position += b2Vec3(-Dog_Scale *0.375f, Dog_Scale*0.125f, 0);
                break;
            case 270:
                dog[2]->transform.position += b2Vec3(-Dog_Scale *0.125f, -Dog_Scale *0.375f, 0);
                break;
            }
        }
        dog[2]->transform.SetScale(Dog_Scale*1.75f, Dog_Scale*1.25f);
        dog[2]->transform.rotation = dog[2]->Next_Rotation;
        dog[2]->sprite.SetFrame(3);
        dog[2]->sprite.SetSpeed(6.f);
        dog[2]->sprite.activeAnimation = true;
        dog[2]->Is_Animated = true;
        dog[2]->customPhysics.AllocateBody(Current_world);
        
        return true;        //delete tail
    }
    else   //For shorten dog[1](body)  and move dog[0](tail)  along with dog[1]
    {
        i = dog.begin();
        (*i)->transform.position += {(*i)->Direction.x, (*i)->Direction.y, 0};

        (*i)->customPhysics.AllocateBody(Current_world);



        const b2Vec2 scale_val2 = { abs((*i)->Direction.x) ,abs((*i)->Direction.y) };
        i = dog.begin() + 1;

        const auto Original_Scale = (*i)->transform.GetScale();
        (*i)->transform.SetScale(Original_Scale - scale_val2);

        if (i != dog.end() - 2) //if dog size is 3(exception! initial situation)
            (*i)->transform.position += b2Vec3{(*i)->Direction.x/2.f, (*i)->Direction.y/2.f, 0};
        else if (i == dog.end() - 2 && dog[2]->Is_Animated == true)
        {
            (*i)->transform.position += b2Vec3{ (*i)->Direction.x/2.f, (*i)->Direction.y/2.f, 0 };
        }
        (*i)->customPhysics.AllocateBody(Current_world);
    }

    return false;
}


//chnage head to rotating animation
void Dog_Control::Rotate_Dog(SDL_Renderer* m_renderer, b2Vec2 rotate_direction)
{
    if (dog[dog.size() - 1]->Is_Animated == true)
        return;
    if (rotate_direction==b2Vec2(-1.f,0.f))
    {
        if (Head_Heading == b2Vec2{ -1.f, 0.f } || Head_Heading == b2Vec2{ 1.f, 0.f })
            return;
        Head_Heading = { -1.f,0.f };
            
        auto i = dog.end() - 1;
        //Curve part
        if ((*i)->Direction == b2Vec2{ 0.f, 1.f })
        {
            (*i)->transform.position += b2Vec3(-Dog_Scale *0.375f, Dog_Scale*0.125f, 0.f);
            (*i)->transform.rotation = 0.f;
            (*i)->Rotate_Image_Direction = Left;
            (*i)->sprite.Free();
            std::string temp = Dog_head_ani_flip_path + std::to_string(dog_ver) + ".png";
            (*i)->sprite.LoadImage(temp.c_str(), m_renderer);
        }
        else if ((*i)->Direction == b2Vec2{ 0.f, -1.f })
        {
            (*i)->transform.position += b2Vec3(-Dog_Scale *0.375f, -Dog_Scale *0.125f, 0.f);
            (*i)->transform.rotation = 180.f;
            (*i)->Rotate_Image_Direction = Right;
            (*i)->sprite.Free();
            std::string temp = Dog_head_ani_path + std::to_string(dog_ver) + ".png";
            (*i)->sprite.LoadImage(temp.c_str(), m_renderer);
        }
        (*i)->transform.SetScale(Dog_Scale*1.75f, Dog_Scale*1.25f);
        (*i)->sprite.SetFrame(3);
        (*i)->sprite.SetSpeed(6.f);
        (*i)->sprite.activeAnimation = true;
        (*i)->Is_Animated = true;
        (*i)->Next_Rotation = 90;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;

    }
    else if (rotate_direction == b2Vec2(1.f, 0.f))
    {
        if (Head_Heading == b2Vec2{ -1.f, 0.f } || Head_Heading == b2Vec2{ 1.f, 0.f })
            return;
        Head_Heading = { 1.f,0.f };

        auto i = dog.end() - 1;
        //Curve part
        if ((*i)->Direction == b2Vec2{ 0.f, 1.f })
        {
            (*i)->transform.position += b2Vec3(Dog_Scale*0.375f, Dog_Scale*0.125f, 0.f);
            (*i)->transform.rotation = 0.f;
            (*i)->Rotate_Image_Direction = Right;
            (*i)->sprite.Free();
            std::string temp = Dog_head_ani_path + std::to_string(dog_ver) + ".png";
            (*i)->sprite.LoadImage(temp.c_str(), m_renderer);
        }
        else if ((*i)->Direction == b2Vec2{ 0.f, -1.f })
        {
            (*i)->transform.position += b2Vec3(Dog_Scale*0.375f, -Dog_Scale *0.125f, 0.f);
            (*i)->transform.rotation = 180.f;
            (*i)->Rotate_Image_Direction = Left;
            (*i)->sprite.Free();
            std::string temp = Dog_head_ani_flip_path + std::to_string(dog_ver) + ".png";
            (*i)->sprite.LoadImage(temp.c_str(), m_renderer);
        }
        (*i)->transform.SetScale(Dog_Scale*1.75f, Dog_Scale*1.25f);
        (*i)->sprite.SetFrame(3);
        (*i)->sprite.SetSpeed(6.f);
        (*i)->sprite.activeAnimation = true;
        (*i)->Is_Animated = true;
        (*i)->Next_Rotation = 270;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;

    }
    else if (rotate_direction == b2Vec2(0.f, -1.f))
    {
        if (Head_Heading == b2Vec2{ 0.f ,-1.f } || Head_Heading == b2Vec2{ 0.f, 1.f })
            return;
        Head_Heading = b2Vec2{ 0.f,-1.f };

        //Curve part
        auto i = dog.end() - 1;
        if ((*i)->Direction == b2Vec2{ 1.f, 0.f })
        {
            (*i)->transform.position += b2Vec3(Dog_Scale*0.125f, -Dog_Scale *0.375f, 0.f);
            (*i)->transform.rotation = 270.f;
            (*i)->Rotate_Image_Direction = Right;
            (*i)->sprite.Free();
            std::string temp = Dog_head_ani_path + std::to_string(dog_ver) + ".png";
            (*i)->sprite.LoadImage(temp.c_str(), m_renderer);
            (*i)->Next_Rotation = 90;
        }
        else if ((*i)->Direction == b2Vec2{ -1.f, 0.f })
        {
            (*i)->transform.position += b2Vec3(-Dog_Scale *0.125f, -Dog_Scale *0.375f, 0.f);
            (*i)->transform.rotation = 90.f;
            (*i)->Rotate_Image_Direction = Left;
            (*i)->sprite.Free();
            std::string temp = Dog_head_ani_flip_path + std::to_string(dog_ver) + ".png";
            (*i)->sprite.LoadImage(temp.c_str(), m_renderer);
            (*i)->Next_Rotation = 180;
        }
        (*i)->transform.SetScale(Dog_Scale*1.75f, Dog_Scale*1.25f);
        (*i)->sprite.SetFrame(3);
        (*i)->sprite.SetSpeed(6.f);
        (*i)->sprite.activeAnimation = true;
        (*i)->Is_Animated = true;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;
        ////
    }
    else if (rotate_direction == b2Vec2(0.f, 1.f))
    {
        if (Head_Heading == b2Vec2{ 0.f ,-1.f } || Head_Heading == b2Vec2{ 0.f, 1.f })
            return;
        Head_Heading = b2Vec2{ 0.f,1.f };

        //Curve part
        auto i = dog.end() - 1;
        if ((*i)->Direction == b2Vec2{ 1.f, 0.f })
        {
            (*i)->transform.position += b2Vec3(Dog_Scale*0.125f, Dog_Scale*0.375f,0.f);
            (*i)->transform.rotation = 270.f;
            (*i)->Rotate_Image_Direction = Left;
            (*i)->sprite.Free();
            std::string temp = Dog_head_ani_flip_path + std::to_string(dog_ver) + ".png";
            (*i)->sprite.LoadImage(temp.c_str(), m_renderer);
        }
        else if ((*i)->Direction == b2Vec2{ -1.f, 0.f })
        {
            (*i)->transform.position += b2Vec3(-Dog_Scale *0.125f, Dog_Scale*0.375f, 0.f);
            (*i)->transform.rotation = 90.f;
            (*i)->Rotate_Image_Direction = Right;
            (*i)->sprite.Free();
            std::string temp = Dog_head_ani_path + std::to_string(dog_ver) + ".png";
            (*i)->sprite.LoadImage(temp.c_str(), m_renderer);
        }
        (*i)->transform.SetScale(Dog_Scale*1.75f, Dog_Scale*1.25f);
        (*i)->sprite.SetFrame(3);
        (*i)->sprite.SetSpeed(6.f);
        (*i)->sprite.activeAnimation = true;
        (*i)->Is_Animated = true;

        (*i)->Next_Rotation = 0;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;

        ////
    }
    dog[dog.size() - 1]->customPhysics.AllocateBody(Current_world);

    dog[dog.size() - 1]->sound.Free();
    dog[dog.size() - 1]->sound.LoadSound("sound/dog_rotate.mp3");
    dog[dog.size() - 1]->sound.SetVolume(100);
    dog[dog.size() - 1]->sound.Play();

    return;
}

void Dog_Control::Extend_Dog(b2World* physics_world)
{
    if (dog[0]->Is_Animated == false)
    {
        dog[0]->transform.position -= {dog[0]->Direction.x, dog[0]->Direction.y, 0};
        dog[0]->customPhysics.AllocateBody(physics_world);

        dog[1]->transform.position -= {dog[1]->Direction.x / 2.0f, dog[1]->Direction.y / 2.0f, 0};
        b2Vec2 abs_scale = b2Vec2(abs(dog[1]->Direction.x), abs(dog[1]->Direction.y));
        
        const auto Original_Scale = dog[1]->transform.GetScale();
        dog[1]->transform.SetScale(Original_Scale + abs_scale);
        dog[1]->customPhysics.AllocateBody(physics_world);
    }
}

void Dog_Control::Set_Extend_Length(int Long)
{
	Extend_Length = Long;
}

bool Dog_Control::Make_Head(SDL_Renderer * m_renderer)
{
    if (Head_Heading == b2Vec2(-1.f, 0.f))
    {
        auto i = dog.end() - 1;
        //Curve part
        if ((*i)->Direction == b2Vec2{ 0.f, 1.f })
        {
            (*i)->transform.position -= b2Vec3(-Dog_Scale *0.375f, Dog_Scale*0.125f, 0.f);
            (*i)->transform.rotation = 270;
        }
        else if ((*i)->Direction == b2Vec2{ 0.f, -1.f })
        {
            (*i)->transform.position -= b2Vec3(-Dog_Scale *0.375f, -Dog_Scale *0.125f, 0.f);
            (*i)->transform.rotation = 180;
        }
        const float Original_Scale_x = (*i)->transform.GetScale().x;
        const float Original_Scale_y = (*i)->transform.GetScale().y;
        (*i)->transform.SetScale(Original_Scale_x*4.f / 7.f, Original_Scale_y*4.f / 5.f);
        (*i)->Direction = Head_Heading;
        (*i)->sprite.Free();
        (*i)->sprite.LoadImage("texture/Character/Dog_Curve.png", m_renderer);
        (*i)->Next_Rotation = 90;

        (*i)->sprite.SetFrame(1);
        (*i)->sprite.activeAnimation = false;
        (*i)->Is_Animated = false;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;
        (*i)->customPhysics.AllocateBody(Current_world);

        //New Body Part///////////
        dog.push_back(new Dog);
        i = dog.end() - 1;
        (*i)->Direction = Head_Heading;
        (*i)->SetName("dog_body");
        (*i)->transform.position = (*(i - 1))->transform.position + b2Vec3{ -(Dog_Scale - 3.f) / 2.f,0.f,0.f };
        (*i)->transform.SetScale(-2, Dog_Scale);
        (*i)->transform.rotation = 180;
        (*i)->sprite.color = WHITE;
        (*i)->sprite.Free();
        (*i)->sprite.LoadImage("texture/Character/Dog_Body.png", m_renderer);
        (*i)->customPhysics.pOwnerTransform = &(*i)->transform;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;
        (*i)->customPhysics.bodyShape = CustomPhysics::BOX;
        (*i)->customPhysics.AllocateBody(Current_world);
		(*i)->customPhysics.AllocateBody(Current_world);

        //New Head Part
        dog.push_back(new Dog);
        i = dog.end() - 1;
        (*i)->Direction = Head_Heading;
        (*i)->SetName("dog");
        (*i)->transform.position = (*(i - 1))->transform.position + b2Vec3{ -(Dog_Scale - 2.f) / 2.f,0.f,0.f };
        (*i)->transform.SetScale(Dog_Scale, Dog_Scale);
        (*i)->transform.rotation = 90;
        (*i)->sprite.color = WHITE;
        (*i)->sprite.Free();
        std::string temp = Dog_head_path + std::to_string(dog_ver) + ".png";
        (*i)->sprite.LoadImage(temp.c_str(), m_renderer);
        (*i)->customPhysics.pOwnerTransform = &(*i)->transform;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;
        (*i)->customPhysics.bodyShape = CustomPhysics::BOX;
        (*i)->customPhysics.AllocateBody(Current_world);

        return true;
    }
    else if (Head_Heading == b2Vec2(1.f, 0.f))
    {
        //Curve part
        auto i = dog.end() - 1;
        if ((*i)->Direction == b2Vec2{ 0.f, 1.f })
        {
            (*i)->transform.position -= b2Vec3(Dog_Scale*0.375f, Dog_Scale*0.125f, 0.f);
            (*i)->transform.rotation = 0;
        }
        else if ((*i)->Direction == b2Vec2{ 0.f, -1.f })
        {
            (*i)->transform.position -= b2Vec3(Dog_Scale*0.375f, -Dog_Scale *0.125f, 0.f);
            (*i)->transform.rotation = 90;
        }

        const float Original_Scale_x = (*i)->transform.GetScale().x;
        const float Original_Scale_y = (*i)->transform.GetScale().y;
        (*i)->transform.SetScale(Original_Scale_x*4.f / 7.f, Original_Scale_y*4.f / 5.f);
        (*i)->Direction = Head_Heading;
        (*i)->sprite.Free();
        (*i)->sprite.LoadImage("texture/Character/Dog_Curve.png", m_renderer);
        (*i)->Next_Rotation = 270;
        (*i)->sprite.activeAnimation = false;
        (*i)->Is_Animated = false;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;
        (*i)->customPhysics.AllocateBody(Current_world);

        //New Body part
        dog.push_back(new Dog);
        i = dog.end() - 1;
        (*i)->Direction = Head_Heading;
        (*i)->SetName("dog_body");
        (*i)->transform.position = (*(i - 1))->transform.position + b2Vec3{ (Dog_Scale - 2.f) / 2.f,0.f,0.f };
        (*i)->transform.SetScale(-2, Dog_Scale);
        (*i)->transform.rotation = 180;
        (*i)->sprite.color = WHITE;
        (*i)->sprite.Free();
        (*i)->sprite.LoadImage("texture/Character/Dog_Body.png", m_renderer);
        (*i)->customPhysics.pOwnerTransform = &(*i)->transform;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;
        (*i)->customPhysics.bodyShape = CustomPhysics::BOX;
        (*i)->customPhysics.AllocateBody(Current_world);
		(*i)->customPhysics.AllocateBody(Current_world);
        //New Head part
        dog.push_back(new Dog);
        i = dog.end() - 1;
        (*i)->Direction = Head_Heading;
        (*i)->SetName("dog");
        (*i)->transform.position = (*(i - 1))->transform.position + b2Vec3{ (Dog_Scale - 3.f) / 2.f,0.f,0.f };
        (*i)->transform.SetScale(Dog_Scale, Dog_Scale);
        (*i)->transform.rotation = 270;
        (*i)->sprite.color = WHITE;
        (*i)->sprite.Free();
        std::string temp = Dog_head_path + std::to_string(dog_ver) + ".png";
        (*i)->sprite.LoadImage(temp.c_str(), m_renderer);
        (*i)->customPhysics.pOwnerTransform = &(*i)->transform;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;
        (*i)->customPhysics.bodyShape = CustomPhysics::BOX;
        (*i)->customPhysics.AllocateBody(Current_world);

        return true;
    }
    else if (Head_Heading == b2Vec2(0.f, -1.f))
    {
        //Curve part
        auto i = dog.end() - 1;
        if ((*i)->Direction == b2Vec2{ 1.f, 0.f })
        {
            (*i)->transform.position -= b2Vec3(Dog_Scale*0.125f, -Dog_Scale *0.375f, 0.f);
            (*i)->transform.rotation = -90;
        }
        else if ((*i)->Direction == b2Vec2{ -1.f, 0.f })
        {
            (*i)->transform.position -= b2Vec3(-Dog_Scale *0.125f, -Dog_Scale *0.375f, 0.f);
            (*i)->transform.rotation = 0;
        }
        const float Original_Scale_x = (*i)->transform.GetScale().x;
        const float Original_Scale_y = (*i)->transform.GetScale().y;
        (*i)->transform.SetScale(Original_Scale_x*4.f / 7.f, Original_Scale_y*4.f / 5.f);
        (*i)->Direction = Head_Heading;
        (*i)->Next_Rotation = 180;
        (*i)->sprite.Free();
        (*i)->sprite.LoadImage("texture/Character/Dog_Curve.png", m_renderer);
        (*i)->sprite.activeAnimation = false;
        (*i)->Is_Animated = false;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;
        (*i)->customPhysics.AllocateBody(Current_world);

        //New Body part
        dog.push_back(new Dog);
        i = dog.end() - 1;
        (*i)->transform.position = (*(i - 1))->transform.position + b2Vec3{ 0.f,-(Dog_Scale - 2.f) / 2.f,0.f };
        (*i)->Direction = Head_Heading;
        (*i)->SetName("dog_body");
        (*i)->transform.SetScale(Dog_Scale, -2);
        (*i)->transform.rotation = 180;
        (*i)->sprite.color = WHITE;
        (*i)->sprite.Free();
        (*i)->sprite.LoadImage("texture/Character/Dog_Body_2.png", m_renderer);
        (*i)->customPhysics.pOwnerTransform = &(*i)->transform;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;
        (*i)->customPhysics.bodyShape = CustomPhysics::BOX;
        (*i)->customPhysics.AllocateBody(Current_world);
		(*i)->customPhysics.AllocateBody(Current_world);
        //New Head part
        dog.push_back(new Dog);
        i = dog.end() - 1;
        (*i)->transform.position = (*(i - 1))->transform.position + b2Vec3{ 0.f,-(Dog_Scale - 3.f) / 2.f,0.f };
        (*i)->Direction = Head_Heading;
        (*i)->SetName("dog");
        (*i)->transform.SetScale(Dog_Scale, Dog_Scale);
        (*i)->transform.rotation = 180;
        (*i)->sprite.color = WHITE;
        (*i)->sprite.Free();
        std::string temp = Dog_head_path + std::to_string(dog_ver) + ".png";
        (*i)->sprite.LoadImage(temp.c_str(), m_renderer);
        (*i)->customPhysics.pOwnerTransform = &(*i)->transform;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;
        (*i)->customPhysics.bodyShape = CustomPhysics::BOX;
        (*i)->customPhysics.AllocateBody(Current_world);

        return true;
    }
    else if (Head_Heading == b2Vec2(0.f, 1.f))
    {
        //Curve part
        auto i = dog.end() - 1;
        if ((*i)->Direction == b2Vec2{ 1.f, 0.f })
        {
            (*i)->transform.position -= b2Vec3(Dog_Scale*0.125f, Dog_Scale*0.375f, 0.f);
            (*i)->transform.rotation = 180;
        }
        else if ((*i)->Direction == b2Vec2{ -1.f, 0.f })
        {
            (*i)->transform.position -= b2Vec3(-Dog_Scale *0.125f, Dog_Scale*0.375f, 0.f);
            (*i)->transform.rotation = 90;
        }
        const float Original_Scale_x = (*i)->transform.GetScale().x;
        const float Original_Scale_y = (*i)->transform.GetScale().y;
        (*i)->transform.SetScale(Original_Scale_x*4.f / 7.f, Original_Scale_y*4.f / 5.f);
        (*i)->Direction = Head_Heading;
        (*i)->Next_Rotation = 0;
        (*i)->sprite.Free();
        (*i)->sprite.LoadImage("texture/Character/Dog_Curve.png", m_renderer);
        (*i)->sprite.activeAnimation = false;
        (*i)->Is_Animated = false;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;
        (*i)->customPhysics.AllocateBody(Current_world);

        //New Body part
        dog.push_back(new Dog);
        i = dog.end() - 1;
        (*i)->transform.position = (*(i - 1))->transform.position + b2Vec3{ 0.f,(Dog_Scale - 3.f) / 2.f,0.f };
        (*i)->Direction = Head_Heading;
        (*i)->SetName("dog_body");
        (*i)->transform.SetScale(Dog_Scale, -2);
        (*i)->transform.rotation = 180;
        (*i)->sprite.color = WHITE;
        (*i)->sprite.Free();
        (*i)->sprite.LoadImage("texture/Character/Dog_Body_2.png", m_renderer);
        (*i)->customPhysics.pOwnerTransform = &(*i)->transform;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;
        (*i)->customPhysics.bodyShape = CustomPhysics::BOX;
        (*i)->customPhysics.AllocateBody(Current_world);
        //New Head part
        dog.push_back(new Dog);
        i = dog.end() - 1;
        (*i)->transform.position = (*(i - 1))->transform.position + b2Vec3{ 0.f,(Dog_Scale- 2.f)/2.f,0.f };
        (*i)->Direction = Head_Heading;
        (*i)->SetName("dog");
        (*i)->transform.SetScale(Dog_Scale, Dog_Scale);
        (*i)->transform.rotation = 0;
        (*i)->sprite.color = WHITE;
        (*i)->sprite.Free();
        std::string temp = Dog_head_path + std::to_string(dog_ver) + ".png";
        (*i)->sprite.LoadImage(temp.c_str(), m_renderer);
        (*i)->customPhysics.pOwnerTransform = &(*i)->transform;
        (*i)->customPhysics.bodyType = CustomPhysics::STATIC;
        (*i)->customPhysics.bodyShape = CustomPhysics::BOX;
        (*i)->customPhysics.AllocateBody(Current_world);

        return true;
    }
    return false;
}

void Dog_Control::Erase_Dog_Tail(void)
{
    delete dog[0];
    dog.erase(dog.begin());
}

void Dog_Control::False_Make_New_Head()
{
    Make_New_Head = false;
}

void Dog_Control::False_Make_New_Tail()
{
    Make_New_Tail = false;
}

std::vector<Dog*> Dog_Control::Get_Dog_Address() const
{
    return dog;
}

Dog * Dog_Control::Get_New_Head_Address() const
{
    return dog[dog.size()-1];
}

Dog * Dog_Control::Get_New_Body_Address() const
{
    return dog[dog.size() - 2];
}

bool Dog_Control::Get_Make_New_Head() const
{
    return Make_New_Head;
}

bool Dog_Control::Get_Make_New_Tail() const
{
    return Make_New_Tail;
}

b2Vec3 Dog_Control::Get_Head_Heading()
{
    b2Vec3 result = { Head_Heading.x*3,Head_Heading.y*3,0 };
    return result;
}

void Dog_Control::Close_Dog()
{
    
    for(auto i=dog.begin();i!=dog.end();)
    {
        delete *i;
        i=dog.erase(i);
    }
    dog.clear();
    std::vector<Dog*>abc;
    dog = abc;
}

void Dog_Control::Die(bool dead)
{
    IsDead = dead;
}

void Dog_Control::Make_Dog_Alpha_to_0()
{
    for(auto i:dog)
    {
        (*i).sprite.color.a = 0;
    }
}
