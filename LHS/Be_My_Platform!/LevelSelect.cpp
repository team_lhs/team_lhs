/******************************************************************************/
/*!
\file   LevelSelect.cpp
\par    Project Name : Be_My_Platform!
\author MinUi Lee, SeongWook Shin

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "LevelSelect.h"

#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/filewritestream.h"
#include "rapidjson/filereadstream.h"
#include <cstdio>

using namespace rapidjson;

void LevelSelect::Initialize()
{
    m_game->Level_Star.clear();
    SDL_ShowCursor(false);
    m_backgroundColor = { 220,255,255,255 };

    SDL_ShowCursor(false);

    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0);

    m_useBuiltInPhysics = false;
    b2Vec2 gravity(0.f, 0.0f);
    SetCustomPhysicsWorld(gravity);

    background.sprite.LoadImage("texture/LevelSelect/BackGround.png", m_renderer);
    background.transform.SetScale(State::m_width, State::m_height);
    AddObject(&background);

    errno_t err;
    FILE* fp;
    err = fopen_s(&fp, "level.json", "rb"); // non-Windows use "r"
    if(err!=0)
    {
        level_num = 2;
        boxes.push_back(new Box);
        boxes[0]->Init(0);
        AddObject(boxes[0]->boxAdd());
	m_game->Level_Star.push_back(0);
        boxes.push_back(new Box);
        boxes[1]->Init(1);
        AddObject(boxes[1]->boxAdd());
	m_game->Level_Star.push_back(0);
    }
    else
    {
        char readBuffer[65536];
        FileReadStream is(fp, readBuffer, sizeof(readBuffer));

        Document d;
        d.ParseStream(is);

        level_num = d["Total Level"].GetInt();
        m_game->total_map = level_num;

        const Value& map = d["MapInfo"];
        assert(map.IsArray());
        for (SizeType i = 0; i < map.Size(); i++)
        {
            auto Ob = map[i].GetObject();
            boxes.push_back(new Box);
            boxes[i]->Init(i);
            AddObject(boxes[i]->boxAdd());
            if (Ob.FindMember("Star")->value.GetInt() >= 1)
                AddObject(boxes[i]->starOne());
            if (Ob.FindMember("Star")->value.GetInt() >= 2)
                AddObject(boxes[i]->starTwo());
            if (Ob.FindMember("Star")->value.GetInt() >= 3)
                AddObject(boxes[i]->starThree());

            boxes[i]->Star_Number = Ob.FindMember("Star")->value.GetInt();
			m_game->Level_Star.push_back(Ob.FindMember("Star")->value.GetInt());
        }
        fclose(fp);
    }
    
    BuildAndRegisterBody(backArrow, "arrow", -550.f, 280.f, 70.f, 70.f);
    Mmouse.Mouse_Init(GetCustomPhysicsWorld());
    AddObject(&Mmouse);
}

void LevelSelect::Update(float dt)
{
    Mmouse.Mouse_Update(GetCustomPhysicsWorld());

    if (Mmouse.IsCollidingWith(&backArrow) && m_input->IsTriggered(SDL_BUTTON_LEFT))
        m_game->Change(LV_main);
    //if (m_input->IsTriggered(SDL_SCANCODE_PAGEDOWN))
    //    m_game->Change(LV_main);

    for(int i = 0; i < level_num; i++)
    {
        if (m_input->IsTriggered(SDL_BUTTON_LEFT) && Mmouse.IsCollidingWith(boxes[i]->boxAdd()))
            m_game->Change(LV_one,i);
    }

    Render(dt);
}

void LevelSelect::Close()
{
    errno_t err;
    FILE* fp;
    err = fopen_s(&fp, "level.json", "wb"); // non-Windows use "r"

    char writeBuffer[65536];
    FileWriteStream os(fp, writeBuffer, sizeof(writeBuffer));
    PrettyWriter<FileWriteStream> writer(os);
    writer.StartObject();
    writer.Key("Total Level");
    writer.Int(level_num);
    writer.Key("MapInfo");
    writer.StartArray();
    for (auto i=0;i<level_num;++i)
    {
        writer.StartObject();
        writer.Key("Star");
        writer.Int(boxes[i]->Star_Number);

        writer.EndObject();
    }
    writer.EndArray();
    writer.EndObject();
    fclose(fp);

    ClearBaseState();
}


//////////////////////////////////  B  O  X  ////////////////////////////////////////

void Box::Init(int order)
{
    order_ = order;

    int row = order % 4;
    int column = order / 4;
    float x = -450.f + row * 300.f;
    float y = 175.f - column * 177.f;

    box.transform.position = { x, y, 0.f };
    box.transform.SetScale(130.f, 130.f);

    // image... �Ф�
    switch (order_)
    {
    case 0:
        box.sprite.LoadImage("texture/LevelSelect/One.png", State::m_renderer); break;
    case 1:
        box.sprite.LoadImage("texture/LevelSelect/Two.png", State::m_renderer); break;
    case 2:
        box.sprite.LoadImage("texture/LevelSelect/Three.png", State::m_renderer); break;
    case 3:
        box.sprite.LoadImage("texture/LevelSelect/four.png", State::m_renderer); break;
    case 4:
        box.sprite.LoadImage("texture/LevelSelect/five.png", State::m_renderer); break;
    case 5:
        box.sprite.LoadImage("texture/LevelSelect/six.png", State::m_renderer); break;
    case 6:
        box.sprite.LoadImage("texture/LevelSelect/seven.png", State::m_renderer); break;
    case 7:
        box.sprite.LoadImage("texture/LevelSelect/eight.png", State::m_renderer); break;
    case 8:
        box.sprite.LoadImage("texture/LevelSelect/nine.png", State::m_renderer); break;
    case 9:
        box.sprite.LoadImage("texture/LevelSelect/ten.png", State::m_renderer); break;
    case 10:
        box.sprite.LoadImage("texture/LevelSelect/eleven.png", State::m_renderer); break;
    case 11:
        box.sprite.LoadImage("texture/LevelSelect/twelve.png", State::m_renderer); break;
    default:
        box.sprite.LoadImage("texture/LevelSelect/Button.png", State::m_renderer); break;
    }

    star1.transform.position = { x - 50.0f,y - 60.0f, 0.f };
    star1.transform.SetScale(50.0f, 50.0f);
    star1.sprite.LoadImage("texture/Button/Icon_Star.png", State::m_renderer);
    star1.sprite.color = { 255, 216, 0, 255 };

    star2.transform.position = { x ,y - 60.0f, 0.f };
    star2.transform.SetScale(50.0f, 50.0f);
    star2.sprite.LoadImage("texture/Button/Icon_Star.png", State::m_renderer);
    star2.sprite.color = { 255, 216, 0, 255 };

    star3.transform.position = { x + 50.0f,y - 60.0f, 0.f };
    star3.transform.SetScale(50.0f, 50.0f);
    star3.sprite.LoadImage("texture/Button/Icon_Star.png", State::m_renderer);
    star3.sprite.color = { 255, 216, 0, 255 };
}

CustomBaseObject& LevelSelect::BuildAndRegisterBody(CustomBaseObject &body, std::string name, float positionX, float positionY, float size_x, float size_y)
{
    body.SetName(name.c_str());
    body.transform.position.Set(positionX, positionY, 0.f);
    body.transform.SetScale(size_x, size_y);

    body.sprite.LoadImage("texture/Button/Play.png", m_renderer);
    body.transform.rotation = 180.f;
    body.sprite.color = { 255, 255, 255, 255 };

    body.customPhysics.GenerateBody(GetCustomPhysicsWorld(), &body.transform);

    AddObject(&body);

    return body;
}