/******************************************************************************/
/*!
\file   LevelOne.h
\par    Project Name : Be_My_Platform!
\author Primary Author: SeongWook Shin, Secondary Author: Minui Lee, Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "engine\State.h"
#include "Dog_Control.h"
#include "Chick.h"
#include "Timer.h"
#include "Option.h"
#include "Mouse.h"
#include "Instruction.h"

class LevelOne : public State
{
    friend class Game;

protected:

    LevelOne() : State() {};
    ~LevelOne() override {};

    void Initialize() override;
    void Update(float dt) override;
    void Close() override;

    // Set the objects which were stored in json file
    void Set_CustomObject_Vector(void);
    // Set the hud objects
    void Set_Hud_Object(CustomBaseObject& object, const char *name, b2Vec3 position, b2Vec2 scale);
    // Set the sound object
    void Set_Sound(CustomBaseObject& object, const char * file_path, int volume = 20);
    
    // Initialize the option
    void InitializeOption();
private:
    // for cheat code: the chick never die
    bool No_Death = false;
    
    // number of current map
    int Current_Map_Number=0;

    const int period = 180;
    float ellapsedtime = 0.0f;
    
    // Characters
    Dog_Control* dog_control;
    Chick chick;
    float past_y = 0.f;
    bool Past_W_Pressed = false;

    // All objects in the stage
    std::vector<CustomBaseObject*>CustomObject;

    // HUD objects
    Timer timer;
    Option option;
    CustomBaseObject HUD;
    Instruction instruction;
    CustomBaseObject Option_ico, Pause_ico, Replay_ico;

    // count is used for timer for update
    int count = 0;
    // check for the instruction 
    bool start_int = true;

    // Background
    CustomBaseObject back;

    // Mouse
    Mouse mouse;
    float mouse_pos_y = 0.f;
    float timer_for_mouse = 0.f;

    // moving map or not
    bool FixedCamera = false;

    // number of star which the player earn in the stage
    int starnum = 0;

    // Check Status
    bool pause_pause = false;
    bool IsPause = false;
    bool Is_Restart = false;
    bool Is_Clear = false;
    float Clear_ellapsedtime = 0.0f;

    bool is_dog_crying = false;
    bool Is_Chick_Dead = false;
    bool Is_Timer_On = false;
    float Dead_ellapsedtime = 0.0f;

    // For SOUND: when the chick stands on the dog
    CustomBaseObject chick_step_dog;
    // For SOUND: when the player earn this
    CustomBaseObject star, item;
};
