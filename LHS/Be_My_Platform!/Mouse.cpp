/******************************************************************************/
/*!
\file   Mouse.cpp
\par    Project Name : Be_My_Platform!
\author MinUi Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Mouse.h"
#include "engine/State.h" // State::m_renderer
#include "Game.h"

void Mouse::Mouse_Init(b2World*)
{
    transform.position.Set(0.f, 0.f, 0.f);
    transform.SetScale(30.f, 35.f);
    sprite.LoadImage("texture/Button/mouse.png", State::m_renderer);
    sprite.isHud = true;
}

void Mouse::Mouse_Update(b2World*)
{
    sprite.color.a = 255;
    transform.position.Set(State::m_input->GetMousePos().x - Game::SCREEN_WIDTH / 2.0f, -(State::m_input->GetMousePos().y - Game::SCREEN_HEIGHT / 2.0f), 0.f);
}

bool Mouse::IsCollidingWith(CustomBaseObject* C2)
{
    b2Vec2 C1_a = { transform.position.x - transform.GetScale().x / 2.0f, transform.position.y + transform.GetScale().y / 2.0f };
    b2Vec2 C1_b = { transform.position.x + transform.GetScale().x / 2.0f, transform.position.y + transform.GetScale().y / 2.0f };
    b2Vec2 C1_c = { transform.position.x - transform.GetScale().x / 2.0f, transform.position.y};

    b2Vec2 C2_a = { C2->transform.position.x - C2->transform.GetScale().x / 2.0f, C2->transform.position.y + C2->transform.GetScale().y / 2.0f };
    b2Vec2 C2_b = { C2->transform.position.x + C2->transform.GetScale().x / 2.0f, C2->transform.position.y + C2->transform.GetScale().y / 2.0f };
    b2Vec2 C2_c = { C2->transform.position.x - C2->transform.GetScale().x / 2.0f, C2->transform.position.y - C2->transform.GetScale().y / 2.0f };

    if(C2->customPhysics.bodyShape == CustomPhysics::BOX)
    {
        if (C1_b.x >= C2_a.x && C1_a.x <= C2_b.x
            && C1_c.y <= C2_a.y && C1_a.y >= C2_c.y)
            return true;
    }
    else
    {
        float x = transform.position.x - C2->transform.position.x;
        float y = transform.position.y - C2->transform.position.y;

        float distance = sqrtf(x*x + y * y);
        if (distance <= C2->customPhysics.radius)
            return true;
    }
    return false;
}

bool Mouse::IsCollidingWith(Object* C2)
{
    b2Vec2 C1_a = { transform.position.x - transform.GetScale().x / 2.0f, transform.position.y + transform.GetScale().y / 2.0f };
    b2Vec2 C1_b = { transform.position.x + transform.GetScale().x / 2.0f, transform.position.y + transform.GetScale().y / 2.0f };
    b2Vec2 C1_c = { transform.position.x - transform.GetScale().x / 2.0f, transform.position.y};

    b2Vec2 C2_a = { C2->transform.position.x - C2->transform.GetScale().x / 2.0f, C2->transform.position.y + C2->transform.GetScale().y / 2.0f };
    b2Vec2 C2_b = { C2->transform.position.x + C2->transform.GetScale().x / 2.0f, C2->transform.position.y + C2->transform.GetScale().y / 2.0f };
    b2Vec2 C2_c = { C2->transform.position.x - C2->transform.GetScale().x / 2.0f, C2->transform.position.y - C2->transform.GetScale().y / 2.0f };

    if (C2->physics.bodyShape == CustomPhysics::BOX)
    {
        if (C1_b.x >= C2_a.x && C1_a.x <= C2_b.x
            && C1_c.y <= C2_a.y && C1_a.y >= C2_c.y)
            return true;
    }
    else
    {
        float x = transform.position.x - C2->transform.position.x;
        float y = transform.position.y - C2->transform.position.y;

        float distance = sqrtf(x*x + y * y);
        if (distance <= C2->physics.radius)
            return true;
    }
    return false;
}