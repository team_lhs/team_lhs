/******************************************************************************/
/*!
\file   Credit.h
\par    Project Name : Be_My_Platform!
\author Minui Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#pragma once
#include "engine\State.h"
#include "engine\Object.h"
#include "CustomBaseObject.h"
#include "Mouse.h"

class Credit : public State
{
    friend class Game;

protected:

    Credit() : State() {};
    ~Credit() override {};

    void Initialize() override;
    void Update(float dt) override;
    void Close() override;

private:
    CustomBaseObject credit, backArrow, background;
    Mouse Mmouse;

};