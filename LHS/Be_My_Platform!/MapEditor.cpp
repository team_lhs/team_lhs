/******************************************************************************/
/*!
\file   MapEditor.cpp
\par    Project Name : Be_My_Platform!
\author Primary author: SeongWook Shin, Secondary author: Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "MapEditor.h"
#include "CommonLevel.h"

#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/filewritestream.h"
#include "rapidjson/filereadstream.h"
#include <cstdio>


using namespace rapidjson;


const SDL_Color GREY_GREEN = { 150,200,125,120 };
const SDL_Color OPAQUE = { 255, 255, 255, 125 };


void MapEditor::Initialize()
{
    DogExist = false;
    ChickExist = false;
    // Set Camera
    // Tell the camera the size of the window
    Total_Obejct_Number = 0;

    camera.Initialize(int(State::m_width), int(State::m_height));
    // Set position of the camera
    camera.position.Set(0, 0, -500);

    mainFont = TTF_OpenFont("font/JaykopDot.ttf", 48);
    m_useBuiltInPhysics = false;

    b2Vec2 gravity(0.f, 0.f);
    SetCustomPhysicsWorld(gravity);
    Set_CustomObject_Vector();

    Make_New_Object(Kind_Of_Object::Wall);
}

void MapEditor::Update(float dt)
{
    Editing_Object->transform.position.x = (m_input->GetMousePos().x - Game::SCREEN_WIDTH / 2.f) / 2.f + camera.position.x;
    Editing_Object->transform.position.y = (-m_input->GetMousePos().y + Game::SCREEN_HEIGHT / 2.f) / 2.f + camera.position.y;
    // Panning by pressing arrow keys

    if (m_input->IsPressed(SDL_SCANCODE_KP_4))
        camera.position += {-3.0f, 0, 0};
    if (m_input->IsPressed(SDL_SCANCODE_KP_6))
        camera.position += {3.0f, 0, 0};
    if (m_input->IsPressed(SDL_SCANCODE_KP_8))
        camera.position += {0, 3.0f, 0};
    if (m_input->IsPressed(SDL_SCANCODE_KP_5))
        camera.position += {0, -3.f, 0};

    if (m_input->IsPressed(SDL_SCANCODE_RIGHT))
    {
        b2Vec2 Original_Scale = Editing_Object->transform.GetScale();
        Editing_Object->transform.SetScale(Original_Scale.x + 1.f, Original_Scale.y);
    }
    if (m_input->IsPressed(SDL_SCANCODE_LEFT))
    {
        b2Vec2 Original_Scale = Editing_Object->transform.GetScale();
        Editing_Object->transform.SetScale(Original_Scale.x - 1.f, Original_Scale.y);
    }
    if (m_input->IsPressed(SDL_SCANCODE_DOWN))
    {
        b2Vec2 Original_Scale = Editing_Object->transform.GetScale();
        if (Original_Scale.y < 0.0f)
        {
            Editing_Object->sprite.flip = SDL_FLIP_VERTICAL;
        }
        Editing_Object->transform.SetScale(Original_Scale.x, Original_Scale.y - 1.f);

    }
    if (m_input->IsPressed(SDL_SCANCODE_UP))
    {
        b2Vec2 Original_Scale = Editing_Object->transform.GetScale();
        Editing_Object->transform.SetScale(Original_Scale.x, Original_Scale.y + 1.f);

    }

    if (m_input->IsTriggered(SDL_SCANCODE_1))
        Change_Current_Object(Kind_Of_Object::Wall);
    if (m_input->IsTriggered(SDL_SCANCODE_2))
        Change_Current_Object(Kind_Of_Object::Ground);


    if (m_input->IsTriggered(SDL_SCANCODE_4))
        Change_Current_Object(Kind_Of_Object::ExtendBody);
    if (m_input->IsTriggered(SDL_SCANCODE_5))
        Change_Current_Object(Kind_Of_Object::Goal);
    if (m_input->IsTriggered(SDL_SCANCODE_6))
        Change_Current_Object(Kind_Of_Object::Fire);
    if (m_input->IsTriggered(SDL_SCANCODE_7))
        Change_Current_Object(Kind_Of_Object::Door);
    if (m_input->IsTriggered(SDL_SCANCODE_L))
        Change_Current_Object(Kind_Of_Object::Lever);
    if (m_input->IsTriggered(SDL_SCANCODE_8))
        Change_Current_Object(Kind_Of_Object::Star);
    if (m_input->IsTriggered(SDL_SCANCODE_9))
        Change_Current_Object(Kind_Of_Object::Timer);

    if (m_input->IsTriggered(SDL_SCANCODE_Q) && ChickExist == false)
        Change_Current_Object(Kind_Of_Object::Chick);
    if (m_input->IsTriggered(SDL_SCANCODE_W) && DogExist == false)
        Change_Current_Object(Kind_Of_Object::Dog);

    if (m_input->IsTriggered(SDL_SCANCODE_F))
        Change_Current_Object(Kind_Of_Object::FixedCamera);

    Editing_Object->customPhysics.AllocateBody(GetCustomPhysicsWorld());
    // Must be one of the last functions called at the end of State Update

    if (m_input->IsTriggered(SDL_BUTTON_LEFT))
    {
        if (Editing_Object->GetName() == "Chick")
        {
            if (ChickExist == true)
                Change_Current_Object(Kind_Of_Object::Wall);
            ChickExist = true;
        }
        if (Editing_Object->GetName() == "Dog")
        {
            if (DogExist == true)
                Change_Current_Object(Kind_Of_Object::Wall);
            DogExist = true;
        }

        Editing_Object->sprite.color.a = 255;
        CustomObject.push_back(Editing_Object);
        ++Total_Obejct_Number;

        Make_New_Object((Kind_Of_Object)Editing_Object->kind);
    }
    if (m_input->IsTriggered(SDL_BUTTON_RIGHT))
    {
        UpdateCustomPhysics(dt);

        if (Editing_Object->customPhysics.IsColliding())
        {
            --Total_Obejct_Number;
            CustomBaseObject* Next_Object = Editing_Object->customPhysics.GetCollidingWith();

            for (auto i = CustomObject.begin(); i != CustomObject.end(); ++i)
            {
                if (*i == Next_Object)
                {
                    CustomObject.erase(i);
                    break;
                }
            }
            RemoveObject(Editing_Object);
            Editing_Object->RemoveCustomPhysicsComp(GetCustomPhysicsWorld(), m_customPhysicsList);
            Editing_Object->customPhysics.Free();
            delete Editing_Object;
            Editing_Object = Next_Object;
            Editing_Object->sprite.color.a = 120;
        }
    }
    Render(dt);

    if (m_input->IsTriggered(SDL_SCANCODE_INSERT))
    {
        m_game->Change(LV_one);
    }
    //// Move Level Select by pressing 'page_down' key
    //if (m_input->IsTriggered(SDL_SCANCODE_PAGEDOWN))
    //{
    //    m_game->Change(Level_select);
    //}
}

void MapEditor::Close()
{
    printf("exit\n");
    errno_t err;
    FILE* fp;
    std::string filename = std::to_string(Current_Map_Number);
    filename += ".json";
    err = fopen_s(&fp, filename.c_str(), "wb"); // non-Windows use "r"

    char writeBuffer[65536];
    FileWriteStream os(fp, writeBuffer, sizeof(writeBuffer));
    PrettyWriter<FileWriteStream> writer(os);
    writer.StartObject();
    writer.Key("ToTal Number");
    writer.Int(Total_Obejct_Number);
    writer.Key("ObjectList");
    writer.StartArray();
    int count = 0;
    for (auto i : CustomObject)
    {
        writer.StartObject();
        writer.Key("Number");
        writer.Int(count);
        writer.Key("Kind");
        writer.Int((*i).kind);
        writer.Key("x_pos");
        writer.Double((*i).transform.position.x);
        writer.Key("y_pos");
        writer.Double(floor((*i).transform.position.y + 0.5));
        writer.Key("x_scale");
        writer.Double((*i).transform.GetScale().x);
        writer.Key("y_scale");
        writer.Double((*i).transform.GetScale().y);
        writer.Key("Is_Moving");
        writer.Int((*i).Is_Moving);

        writer.EndObject();
        ++count;
    }
    writer.EndArray();
    writer.EndObject();
    fclose(fp);
    // Deallocate custom physics world
    RemoveCustomPhysicsWorld();

    // Wrap up state
    ClearBaseState();
}

void MapEditor::Set_CustomObject_Vector(void)
{
    for (auto i = CustomObject.begin(); i != CustomObject.end();)
    {
        delete *i;
        i = CustomObject.erase(i);
    }
    errno_t err;
    FILE* fp;
    std::string filename = std::to_string(Current_Map_Number);
    filename += ".json";
    err = fopen_s(&fp, filename.c_str(), "rb"); // non-Windows use "r"
    if (err != 0)
        return;

    char readBuffer[65536];
    FileReadStream is(fp, readBuffer, sizeof(readBuffer));

    Document d;
    d.ParseStream(is);

    const Value& a = d["ObjectList"];
    assert(a.IsArray());
    for (SizeType i = 0; i < a.Size(); i++)
    {
        auto Ob = a[i].GetObject();
        CustomBaseObject* temp_save = new CustomBaseObject;
        temp_save->kind = Ob.FindMember("Kind")->value.GetInt();
        temp_save->transform.position.x =
            static_cast<float>(Ob.FindMember("x_pos")->value.GetDouble());
        temp_save->transform.position.y =
            static_cast<float>(Ob.FindMember("y_pos")->value.GetDouble());
        b2Vec2 Scale_Set;
        Scale_Set.x =
            static_cast<float>(Ob.FindMember("x_scale")->value.GetDouble());
        Scale_Set.y =
            static_cast<float>(Ob.FindMember("y_scale")->value.GetDouble());
        temp_save->Is_Moving = Ob.FindMember("Is_Moving")->value.GetInt();
        temp_save->transform.SetScale(Scale_Set);
        temp_save->sprite.color = { 255,255,255,255 };

        if (temp_save->kind == static_cast<int>(Kind_Of_Object::Chick))
        {
            temp_save->SetName("Chick");
            temp_save->sprite.LoadImage("texture/Character/Chick.png", m_renderer);
            ChickExist = true;
        }
        else if (temp_save->kind == static_cast<int>(Kind_Of_Object::Dog))
        {
            temp_save->SetName("Dog");
            temp_save->sprite.LoadImage("texture/Character/Dog_Map_Edit.png", m_renderer);
            DogExist = true;
        }
        else if (temp_save->kind == static_cast<int>(Kind_Of_Object::Wall))
        {
            temp_save->SetName("Wall");
            temp_save->sprite.color = GREY_GREEN;
            temp_save->sprite.color.a = 255;
            temp_save->sprite.LoadImage("texture/rect.png", m_renderer);
        }
        else if (temp_save->kind == static_cast<int>(Kind_Of_Object::Ground))
        {
            temp_save->SetName("Ground");
            temp_save->sprite.LoadImage("texture/ground.png", m_renderer);
        }
        else if (temp_save->kind == static_cast<int>(Kind_Of_Object::ExtendBody))
        {
            temp_save->SetName("ExtendBody");
            temp_save->sprite.LoadImage("texture/Items/Potion_simple.png", m_renderer);
        }
        else if (temp_save->kind == static_cast<int>(Kind_Of_Object::Goal))
        {
            temp_save->SetName("Goal");
            temp_save->sprite.LoadImage("texture/Items/Door.png", m_renderer);
        }
        else if (temp_save->kind == static_cast<int>(Kind_Of_Object::Fire))
        {
            temp_save->SetName("Fire");
            temp_save->sprite.LoadImage("texture/Items/Fire_Animation.png", m_renderer);
            temp_save->sprite.activeAnimation = true;
            temp_save->sprite.SetFrame(8);
            temp_save->sprite.SetCurrentFrame(true, 1);
        }
        else if (temp_save->kind == static_cast<int>(Kind_Of_Object::Lever))
        {
            temp_save->SetName("Lever");
            temp_save->sprite.LoadImage("texture/Items/SwitchMid.png", m_renderer);
        }
        else if (temp_save->kind == static_cast<int>(Kind_Of_Object::Door))
        {
            temp_save->SetName("Door");
            temp_save->sprite.LoadImage("texture/Items/Thunder_Cloud.png", m_renderer);
        }
        else if (temp_save->kind == static_cast<int>(Kind_Of_Object::Star))
        {
            temp_save->SetName("Star");
            temp_save->sprite.color = { 255, 216, 0, 255 };
            temp_save->sprite.LoadImage("texture/Button/Icon_Star.png", m_renderer);
        }
        else if (temp_save->kind == static_cast<int>(Kind_Of_Object::FixedCamera))
        {
            temp_save->SetName("FixedCamera");
            temp_save->sprite.color = { 255, 255, 0, 255 };
            temp_save->sprite.LoadImage("texture/rect.png", m_renderer);
        }
        else if (temp_save->kind == static_cast<int>(Kind_Of_Object::Timer))
        {
            temp_save->SetName("Timer");
            temp_save->sprite.LoadImage("texture/Items/Timer_Plus.png", m_renderer);
        }

        temp_save->customPhysics.pOwnerTransform = &temp_save->transform;
        temp_save->customPhysics.bodyType = CustomPhysics::DYNAMIC;
        temp_save->customPhysics.bodyShape = CustomPhysics::BOX;
        temp_save->customPhysics.AllocateBody(GetCustomPhysicsWorld());
        temp_save->customPhysics.ActiveGhostCollision(true);

        CustomObject.push_back(temp_save);
        AddObject(temp_save);
        AddCustomPhysicsComponent(temp_save);
        ++Total_Obejct_Number;
    }
    fclose(fp);
}

void MapEditor::Make_New_Object(Kind_Of_Object kind)
{
    Editing_Object = new CustomBaseObject();

    Editing_Object->SetName("wall");
    Editing_Object->transform.position.Set(0, 0, 0);
    Editing_Object->transform.SetScale(36.f, 36.f);

    Editing_Object->sprite.color = GREY_GREEN;
    Editing_Object->sprite.LoadImage("texture/rect.png", m_renderer);

    Editing_Object->customPhysics.pOwnerTransform = &Editing_Object->transform;
    Editing_Object->customPhysics.bodyType = CustomPhysics::DYNAMIC;
    Editing_Object->customPhysics.bodyShape = CustomPhysics::BOX;
    Editing_Object->customPhysics.AllocateBody(GetCustomPhysicsWorld());
    Editing_Object->customPhysics.ActiveGhostCollision(true);

    AddObject(Editing_Object);
    AddCustomPhysicsComponent(Editing_Object);
    if (kind == Kind_Of_Object::Lever)
        Change_Current_Object(Kind_Of_Object::Door);
    else if (kind == Kind_Of_Object::Door)
        Change_Current_Object(Kind_Of_Object::Lever);
    else
        Change_Current_Object(kind);
}

void MapEditor::Change_Current_Object(Kind_Of_Object number)
{
    Editing_Object->sprite.activeAnimation = false;
    Editing_Object->sprite.Free();
    switch (number)
    {
    case Kind_Of_Object::Chick:
        Editing_Object->SetName("Chick");
        Editing_Object->kind = static_cast<int>(Kind_Of_Object::Chick);
        Editing_Object->transform.SetScale(25, 40);
        Editing_Object->sprite.LoadImage("texture/Character/Chick.png", m_renderer);
        Editing_Object->sprite.color = OPAQUE;
        break;
    case Kind_Of_Object::Dog:
        Editing_Object->SetName("Dog");
        Editing_Object->kind = static_cast<int>(Kind_Of_Object::Dog);
        Editing_Object->transform.SetScale(32.f, 32.f);
        Editing_Object->sprite.LoadImage("texture/Character/Dog_Map_Edit.png", m_renderer);
        Editing_Object->sprite.color = OPAQUE;
        break;
    case Kind_Of_Object::Wall:
        Editing_Object->SetName("Wall");
        if (Editing_Object->kind == static_cast<int>(Kind_Of_Object::Wall))
        {
            if(Editing_Object->Is_Moving==0)
                Editing_Object->Is_Moving = 1;
            else
                Editing_Object->Is_Moving = 2;
        }
        else
        {
            Editing_Object->Is_Moving = 0;
        }
        Editing_Object->kind = static_cast<int>(Kind_Of_Object::Wall);
        Editing_Object->transform.SetScale(36.f, 36.f);
        Editing_Object->sprite.color = GREY_GREEN;
        Editing_Object->sprite.LoadImage("texture/rect.png", m_renderer);
        break;
    case Kind_Of_Object::Ground:
        Editing_Object->SetName("Ground");
        if (Editing_Object->kind == static_cast<int>(Kind_Of_Object::Ground))
        {
            if (Editing_Object->Is_Moving == 0)
                Editing_Object->Is_Moving = 1;
            else
                Editing_Object->Is_Moving = 2;
        }
        else
        {
            Editing_Object->Is_Moving = 0;
        }
        Editing_Object->kind = static_cast<int>(Kind_Of_Object::Ground);
        Editing_Object->transform.SetScale(36.f, 36.f);
        Editing_Object->sprite.color = OPAQUE;
        Editing_Object->sprite.LoadImage("texture/ground.png", m_renderer);
        break;
    case Kind_Of_Object::ExtendBody:
        Editing_Object->SetName("ExtendBody");
        if (Editing_Object->kind == static_cast<int>(Kind_Of_Object::ExtendBody))
        {
            if (Editing_Object->Is_Moving == 0)
                Editing_Object->Is_Moving = 1;
            else
                Editing_Object->Is_Moving = 2;
        }
        else
        {
            Editing_Object->Is_Moving = 0;
        }
        Editing_Object->kind = static_cast<int>(Kind_Of_Object::ExtendBody);
        Editing_Object->transform.SetScale(25, 32);
        Editing_Object->sprite.LoadImage("texture/Items/Potion_simple.png", m_renderer);
        Editing_Object->sprite.color = OPAQUE;
        break;
    case Kind_Of_Object::Goal:
        Editing_Object->SetName("Goal");
        if (Editing_Object->kind == static_cast<int>(Kind_Of_Object::Goal))
        {
            if (Editing_Object->Is_Moving == 0)
                Editing_Object->Is_Moving = 1;
            else
                Editing_Object->Is_Moving = 2;
        }
        else
        {
            Editing_Object->Is_Moving = 0;
        }
        Editing_Object->kind = static_cast<int>(Kind_Of_Object::Goal);
        Editing_Object->transform.SetScale(50.0f, 50.0f);
        Editing_Object->sprite.LoadImage("texture/Items/Door.png", m_renderer);
        Editing_Object->sprite.color = OPAQUE;
        break;
    case Kind_Of_Object::Fire:
        Editing_Object->SetName("Fire");
        if (Editing_Object->kind == static_cast<int>(Kind_Of_Object::Fire))
        {
            if (Editing_Object->Is_Moving == 0)
                Editing_Object->Is_Moving = 1;
            else
                Editing_Object->Is_Moving = 2;
        }
        else
        {
            Editing_Object->Is_Moving = 0;
        }
        Editing_Object->kind = static_cast<int>(Kind_Of_Object::Fire);
        Editing_Object->transform.SetScale(60.0f, 60.0f);
        Editing_Object->sprite.LoadImage("texture/Items/Fire_Animation.png", m_renderer);
        Editing_Object->sprite.activeAnimation = true;
        Editing_Object->sprite.SetFrame(8);
        Editing_Object->sprite.SetCurrentFrame(true, 1);
        Editing_Object->sprite.color = OPAQUE;
        break;
    case Kind_Of_Object::Lever:
        Editing_Object->SetName("Lever");
        if (Editing_Object->kind == static_cast<int>(Kind_Of_Object::Lever))
        {
            if (Editing_Object->Is_Moving == 0)
                Editing_Object->Is_Moving = 1;
            else
                Editing_Object->Is_Moving = 2;
        }
        else
        {
            Editing_Object->Is_Moving = 0;
        }
        Editing_Object->kind = static_cast<int>(Kind_Of_Object::Lever);
        Editing_Object->transform.SetScale(25.0f, 25.0f);
        Editing_Object->sprite.LoadImage("texture/Items/SwitchMid.png", m_renderer);
        Editing_Object->sprite.color = OPAQUE;
        break;
    case Kind_Of_Object::Door:
        Editing_Object->SetName("Door");
        if (Editing_Object->kind == static_cast<int>(Kind_Of_Object::Door))
        {
            if (Editing_Object->Is_Moving == 0)
                Editing_Object->Is_Moving = 1;
            else
                Editing_Object->Is_Moving = 2;
        }
        else
        {
            Editing_Object->Is_Moving = 0;
        }
        Editing_Object->kind = static_cast<int>(Kind_Of_Object::Door);
        Editing_Object->transform.SetScale(25.0f, 25.0f);
        Editing_Object->sprite.LoadImage("texture/Items/Thunder_Cloud.png", m_renderer);
        Editing_Object->sprite.color = OPAQUE;
        break;
    case Kind_Of_Object::Star:
        Editing_Object->SetName("Star");
        if (Editing_Object->kind == static_cast<int>(Kind_Of_Object::Star))
        {
            if (Editing_Object->Is_Moving == 0)
                Editing_Object->Is_Moving = 1;
            else
                Editing_Object->Is_Moving = 2;
        }
        else
        {
            Editing_Object->Is_Moving = 0;
        }
        Editing_Object->kind = static_cast<int>(Kind_Of_Object::Star);
        Editing_Object->transform.SetScale(25.0f, 25.0f);
        Editing_Object->sprite.color = { 255, 216, 0, 125 };
        Editing_Object->sprite.LoadImage("texture/Button/Icon_Star.png", m_renderer);
        break;
    case Kind_Of_Object::FixedCamera:
        Editing_Object->SetName("FixedCamera");
        Editing_Object->kind = static_cast<int>(Kind_Of_Object::FixedCamera);
        Editing_Object->transform.SetScale(25.0f, 25.0f);
        Editing_Object->sprite.color = { 255, 255, 0, 125 };
        Editing_Object->sprite.LoadImage("texture/rect.png", m_renderer);
        break;
    case Kind_Of_Object::Timer:
        Editing_Object->SetName("Timer");
        Editing_Object->kind = static_cast<int>(Kind_Of_Object::Timer);
        Editing_Object->transform.SetScale(32, 32);
        Editing_Object->sprite.color = { 255, 255, 0, 125 };
        Editing_Object->sprite.LoadImage("texture/Items/Timer_Plus.png", m_renderer);
        Editing_Object->sprite.color = OPAQUE;
        break;
    }
}