/******************************************************************************/
/*!
\file   Timer.cpp
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Timer.h"
#include "engine\State.h" //State::m_renderer

void Timer::SetRemainTimeString()
{
	std::string minute;
	std::string second;

	// Put remain minutes
	if ((time_remain / 60) < 10)
		minute = '0' + std::to_string(time_remain / 60);
	else
		minute = std::to_string(time_remain / 60);

	// Put remain seconds
	if ((time_remain % 60) < 10)
		second = '0' + std::to_string(time_remain % 60);
	else
		second = std::to_string(time_remain % 60);

	timer = minute + ':' + second;
}

void Timer::InitTimer(TTF_Font* font, float limit_time)
{
        text.Free();
	timer_font = font;
	time_limit = limit_time;

	SetName("Timer");
	transform.position.Set(-515.f, 300.f, 0.0f);
	transform.SetScale(120, 60);

	// Set the text to render out
        text.SetText(State::m_renderer, timer.c_str(), timer_font);
        
	// Set the colors r,g,b,a (0~255)
	text.color.r = 0;
	text.color.g = 0;
	text.color.b = 0;
	text.color.a = 225;
	text.isHud = true;
}

void Timer::SetTimer(float ellapsedtime)
{
	// remain time
	time_remain = int(time_limit - ellapsedtime + time_increase + reset_time);

	// Set the string for timer
	SetRemainTimeString();
        
        text.Free();
        text.SetText(State::m_renderer, timer.c_str(), timer_font);
}

bool Timer::IsTimeOver()
{
	if (time_remain <= 0)
		return true;
	return false;
}

void Timer::IncreaseTime(float time_to_add)
{
	time_increase += time_to_add;
}

void Timer::ResetTimer(float ellapsedtime)
{
    time_increase = 0.0f;
    reset_time = ellapsedtime;
}

int Timer::GetRemainTime()
{
    return time_remain;
}

void Timer::SetRemainTime(float time)
{
    time_remain = (int)time;
}
