/******************************************************************************/
/*!
\file   SplashScreen.h
\par    Project Name : Be_My_Platform!
\author Minui Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "engine\State.h"
#include "engine\Object.h"
#include "CustomBaseObject.h"
#include "Mouse.h"

class SplashScreen : public State
{
    friend class Game;

protected:

    SplashScreen() : State() {};
    ~SplashScreen() override {};

    void Initialize() override;
    void Update(float dt) override;
    void Close() override;

    int order = 0;
    bool isMain = false;

private:
    CustomBaseObject	Logo, titleText;
    CustomBaseObject Earth, characters, startbutton, background;
    Mouse Mmouse;

    float Ellapsed_Time=0.f;

    void colorAmodulate();
    void BuildObject(CustomBaseObject &obj, std::string name, float scaleX, float scaleY);
};