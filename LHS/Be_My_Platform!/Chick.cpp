/******************************************************************************/
/*!
\file   Chick.cpp
\par    Project Name : Be_My_Platform!
\author MinUi Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Chick.h"
#include "engine/State.h" // State::m_renderer
#include <iostream>

void Chick::Chick_Init(b2Vec3 position, b2World* world)
{
    velocity = 5.0f;

    chick.SetName("Chick");
    chick.transform.position.Set(position.x, position.y, position.z);
    chick.transform.SetScale(28, 40);
    chick.sprite.LoadImage("texture/Character/Chick.png", State::m_renderer);
    chick.customPhysics.bodyType = CustomPhysics::DYNAMIC;
    chick.customPhysics.bodyShape = CustomPhysics::BOX;
    chick.customPhysics.GenerateBody(world, &chick.transform);
    chick.customPhysics.GetBody()->GetFixtureList()->SetFriction(0.f);
    soundONOFF = false;
}

void Chick::Chick_update(keys key)
{
    float temp_x_vel = chick.customPhysics.GetVelocity().x;
    float temp_y_vel = chick.customPhysics.GetVelocity().y;
    
    //chick.sound.Play();
    switch(key)
    {
    case KEY_W:
        if ((State::m_input->IsTriggered(SDL_SCANCODE_W) ||
            State::m_input->IsTriggered(SDL_SCANCODE_UP)) &&
            (abs(Past_y - chick.transform.position.y) < 0.01f))
        {
            chick.customPhysics.AddForce(0.f, 9.0f);
            chick.sound.Free();
            chick.sound.LoadSound("sound/Chick_Jump.flac");
            chick.sound.Play();
        }
        break;
    case KEY_A:
        if(Move_Direction==1)
            chick.customPhysics.SetVelocity(-velocity*1.5f, temp_y_vel);
        else
            chick.customPhysics.SetVelocity(-velocity, temp_y_vel);
        break;
    case KEY_D:
        if (Move_Direction == 2)
            chick.customPhysics.SetVelocity(velocity * 1.5f, temp_y_vel);
        else
            chick.customPhysics.SetVelocity(velocity, temp_y_vel);
        break;
    case KEY_W_RELEASED:
        if(temp_y_vel>0.f)
        chick.customPhysics.SetVelocity(temp_x_vel, temp_y_vel/2.f);
        break;
    default: 
        break;
    }
    Past_y = chick.transform.position.y;
}

CustomBaseObject* Chick::chick_address()
{
    return &chick;
}

void Chick::Die(bool dead)
{
    IsDead = dead;
}

void Chick::Finish(bool finish)
{
    IsFinish = finish;
}

void Chick::SpeedUp(float scale)
{
    velocity += scale;
}

bool Chick::IsLose()
{
    return IsDead;
}

bool Chick::IsFinished()
{
    return IsFinish;
}

void Chick::setAnimi(int direction) // -1 = LEFT, 0 = Not Moving, 1 = RIGHT
{
	float curr_vel_x = abs(chick.customPhysics.GetVelocity().x);

	if (direction == -1)
	{
		chick.sprite.Free();
		chick.sprite.LoadImage("texture/Character/Chick_Animi.png", State::m_renderer);
		chick.sprite.SetFrame(3);
		chick.sprite.SetSpeed(4.0f);
		chick.sprite.flip = SDL_FLIP_HORIZONTAL;
		chick.sprite.activeAnimation = true;
	}
	else if (direction == 1)
	{
		chick.sprite.Free();
		chick.sprite.LoadImage("texture/Character/Chick_Animi.png", State::m_renderer);
		chick.sprite.SetFrame(3);
		chick.sprite.SetSpeed(4.0f);
		chick.sprite.flip = SDL_FLIP_NONE;
		chick.sprite.activeAnimation = true;
	}
	
	if(direction == 0 && curr_vel_x <= 1.0f)
	{
		chick.sprite.Free();
		chick.sprite.LoadImage("texture/Character/chick.png", State::m_renderer);
		chick.sprite.flip = SDL_FLIP_NONE;
		chick.sprite.activeAnimation = false;
	}
}

bool Chick::CheckOkay(direction d) //true == cannot do wall riding
{
    if (d == LEFT)
    {
                return true;
    }
    else if(d == RIGHT)
    {
                    return true;
    }
    return false;
}