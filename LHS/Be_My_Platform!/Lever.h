/******************************************************************************/
/*!
\file   Lever.h
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "Item.h"
#include "Door.h"

class Lever : public Item
{

public:
    void Initialize(b2Vec3 position, b2Vec2 scale, b2World* world, Chick* chick_pointer, Dog_Control* dog_control_pointer) override;
    void Update() override;
    
    Door * door;

    bool Is_Sound_On = false;
};