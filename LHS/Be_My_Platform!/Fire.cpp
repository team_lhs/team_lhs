/******************************************************************************/
/*!
\file   Fire.cpp
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Fire.h"
#include "engine/State.h"	// State::m_renderer

void Fire::Initialize(b2Vec3 position, b2Vec2 scale, b2World* world, Chick * chick_pointer, Dog_Control *dog_control_pointer)
{
    // Set pointers
    this_world = world;
    chick = chick_pointer;
    dog_control = dog_control_pointer;

    // Set name
    SetName("Fire");

    // Set transform
    transform.position.Set(position.x, position.y, position.z);
    transform.SetScale(scale.x, scale.y);
    transform.rotation = 0.0f;

    // Set spirte(Animation)
    sprite.LoadImage("texture/Items/Fire_Animation.png", State::m_renderer);
    sprite.activeAnimation = true;
    sprite.SetFrame(8);
    sprite.SetSpeed(15.0f);
    customPhysics.pOwnerTransform = &transform;
    customPhysics.bodyType = CustomPhysics::DYNAMIC;
    customPhysics.bodyShape = CustomPhysics::BOX;
    customPhysics.AllocateBody(world);
    customPhysics.GetBody()->SetGravityScale(0.0f);
    customPhysics.ActiveGhostCollision(true);

    /****************** G H O S T *****************/
    // Set transform
	fire_ghost.SetName("Fire_Ghost");
    fire_ghost.transform.position.Set(position.x - scale.x / 40, position.y - scale.y / 5.5f, position.z);
    fire_ghost.transform.SetScale(scale.x / 1.7f, scale.y / 3);
    fire_ghost.transform.rotation = 0.0f;

    //Set sprite
    fire_ghost.sprite.LoadImage("texture/rect.png", State::m_renderer);
    fire_ghost.sprite.color = { 0, 0, 0, 0 };

    // Set customphysics
    fire_ghost.customPhysics.pOwnerTransform = &fire_ghost.transform;
    fire_ghost.customPhysics.bodyType = CustomPhysics::DYNAMIC;
    fire_ghost.customPhysics.bodyShape = CustomPhysics::BOX;
    fire_ghost.customPhysics.AllocateBody(world);
    fire_ghost.customPhysics.ActiveGhostCollision(true);
    fire_ghost.customPhysics.GetBody()->SetGravityScale(0.f);
}

void Fire::Update()
{
    customPhysics.ActiveGhostCollision(true);
    fire_ghost.transform.position.Set(transform.position.x - transform.GetScale().x / 40, transform.position.y - transform.GetScale().y / 5.5f, 0.f);
    fire_ghost.customPhysics.AllocateBody(this_world);
}

CustomBaseObject* Fire::GetFireGhostAddress()
{
    return &fire_ghost;
}