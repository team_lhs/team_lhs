/******************************************************************************/
/*!
\file   Potion.h
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "Item.h"
#include "Timer.h"

class Potion : public Item
{
private:
    // for timer item
    Timer * timer = NULL;
    Kind_Of_Object potion_type = Kind_Of_Object::ExtendBody;

    // when the player use the potion
    void ActivePotion(float size);
public:
    void Initialize(b2Vec3 position, b2Vec2 scale, b2World* world, Chick * chick_pointer, Dog_Control *dog_control_pointer) override;
    void Update() override;

    // Set the type of potion
    void Set_PotionType(Kind_Of_Object type);
    // this is used for timer item
    void Set_TimerPointer(Timer* time);
};