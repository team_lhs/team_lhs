/******************************************************************************/
/*!
\file   SplashScreen.cpp
\par    Project Name : Be_My_Platform!
\author Minui Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "SplashScreen.h"
#include "CommonLevel.h"

void SplashScreen::Initialize()
{
    SDL_ShowCursor(false);
    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0);
    m_useBuiltInPhysics = false;
    b2Vec2 gravity(0.f, 0.0f);
    SetCustomPhysicsWorld(gravity);
    mainFont = TTF_OpenFont("font/Default.ttf", 48);

    BuildObject(background, "Background", 1280.f, 720.f);
    BuildObject(Logo, "Logo", State::m_height, State::m_height * 0.3f);

    BuildObject(Earth, "Earth", 800.f, 800.f);
    BuildObject(characters, "Characters", 800.f, 800.f);
    BuildObject(startbutton, "StartButton", 200.f, 200.f);
    BuildObject(titleText, "Title", 1000.f, 180.f);

    Mmouse.Mouse_Init(GetCustomPhysicsWorld());
}

void SplashScreen::Update(float dt)
{
    if (!isMain)
    {
        Ellapsed_Time += dt;
        colorAmodulate();
        switch(order)
        {
        case 0:
            Logo.sprite.color.a += 3;
            break;
        case 1:
            Logo.sprite.color.a -= 3;
            break;
        case 2:
            Logo.sprite.color.a += 3;
            break;
        case 3:
            Logo.sprite.color.a -= 3;
            break;
        case 4:
            isMain = true;
            Logo.sprite.color.a = 0;
            characters.sprite.color.a = 255;
            startbutton.sprite.color.a = 255;
            titleText.sprite.color.a = 255;
            Earth.sprite.color.a = 255;
            titleText.sprite.color.a = 255;
            background.sprite.color.a = 255;
            AddObject(&Mmouse);
            break;
        }
    }
    else
    {
        Mmouse.Mouse_Update(GetCustomPhysicsWorld());
        characters.transform.rotation -= 0.1f;
        Earth.transform.rotation += 0.003f;
        if (Mmouse.IsCollidingWith(&startbutton))
        {
            startbutton.customPhysics.radius = 150.f;
            startbutton.transform.SetScale(300.f, 300.f);
        }
        else
        {
            startbutton.customPhysics.radius = 100.f;
            startbutton.transform.SetScale(200.f, 200.f);
        }

        if (m_input->IsTriggered(SDL_BUTTON_LEFT))
        {
            if (Mmouse.IsCollidingWith(&startbutton) && isMain)
                m_game->Change(LV_main);
        }
    }

    if (Ellapsed_Time>3.f&&m_input->IsAnyTriggered())
        m_game->Change(LV_main);

    Render(dt);
}

void SplashScreen::Close()
{
    ClearBaseState();
}

void SplashScreen::colorAmodulate()
{
    if (Logo.sprite.color.a > 250 && order == 0)
        order = 1;
    if (Logo.sprite.color.a < 10 && order == 1)
    {
        order = 2;
        Logo.sprite.LoadImage("texture/Logo/LHS_logo.png", State::m_renderer);
        Logo.transform.SetScale(State::m_height * 0.4f, State::m_height * 0.7f);
    }
    if (order == 2 && Logo.sprite.color.a > 250)
        order = 3;
    if(order ==3 && Logo.sprite.color.a < 10)
        order = 4;
}

void SplashScreen::BuildObject(CustomBaseObject &obj, std::string name, float scaleX, float scaleY)
{
    const float POSITION_Z = 0.f;
    obj.SetName(name.c_str());
    obj.transform.position.Set(0.0f, -100.0f, POSITION_Z);
    obj.transform.SetScale(scaleX, scaleY);
    obj.sprite.color = {255,255,255,0};
    obj.customPhysics.bodyShape = CustomPhysics::CIRCLE;
    obj.customPhysics.radius = 400.f;

    if (name == "Background")
    {
        obj.transform.position.Set(0.0f, 0.0f, -100.0f);
        obj.sprite.LoadImage("texture/PNG_FILE/Sky.png", State::m_renderer);
    }
    else if(name == "Characters")
    {
        obj.sprite.LoadImage("texture/PNG_FILE/StartScreen_Animi.png", State::m_renderer);
        obj.sprite.SetFrame(2);
        obj.sprite.SetSpeed(10.f);
        obj.transform.rotation = 90.f;
        obj.sprite.activeAnimation = true;
    }
    else if(name == "Earth")
    {
        obj.sprite.LoadImage("texture/PNG_FILE/StartScreen_Background.png", State::m_renderer);
    }
    else if(name == "StartButton")
    {
        obj.sprite.LoadImage("texture/Button/Play_i.png", State::m_renderer);
        obj.customPhysics.radius = 100.f;
    }
    else if(name == "Title")
    {
        obj.transform.position.Set(0.f, 250.f, 0.0f);
        obj.sprite.LoadImage("texture/Logo/Title.png", State::m_renderer);
    }
    else if(name == "Logo")
    {
        obj.transform.position.SetZero();
        obj.transform.SetScale(State::m_height, State::m_height * 0.3f);
        obj.sprite.LoadImage("texture/Logo/Digipen_logo.png", State::m_renderer);
    }

    obj.customPhysics.GenerateBody(GetPhysicsWorld(), &obj.transform);

    AddObject(&obj);
}