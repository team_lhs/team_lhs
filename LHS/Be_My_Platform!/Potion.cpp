/******************************************************************************/
/*!
\file   Potion.cpp
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Potion.h"
#include "engine/State.h"	// State::m_renderer

void Potion::ActivePotion(float size)
{
    // if the potion is extend body potion
    if (potion_type == Kind_Of_Object::ExtendBody)
    {
        // extend the dog's body
        dog_control->Set_Extend_Length(static_cast<int>(size));
    }
    // if the timer item
    else if (potion_type == Kind_Of_Object::Timer)
    {
        // increase the remain time
        timer->IncreaseTime(size);
    }

    // remove the object
    remove = Kind_Of_Death::Object_Death;
}

void Potion::Set_PotionType(Kind_Of_Object type)
{
    // Set type
    potion_type = type;

    // Set name and sprite for each potion
    if (potion_type == Kind_Of_Object::ExtendBody)
    {
        SetName("Potion_StretchBody");
        sprite.LoadImage("texture/Items/Potion_simple.png", State::m_renderer);
    }
    else if (potion_type == Kind_Of_Object::Timer)
    {
        SetName("Potion_TimeIncrease");
        sprite.LoadImage("texture/Items/Timer_Plus.png", State::m_renderer);
    }
}

void Potion::Set_TimerPointer(Timer* time)
{
    // Set the timer pointer for timer item
    timer = time;
}

void Potion::Initialize(b2Vec3 position, b2Vec2 scale, b2World* world, Chick * chick_pointer, Dog_Control *dog_control_pointer)
{
    // Set the pointers
    this_world = world;
    chick = chick_pointer;
    dog_control = dog_control_pointer;
    
    // Set transform
    transform.position.Set(position.x, position.y, position.z);
    transform.SetScale(scale.x, scale.y);
    transform.rotation = 0.0f;
    
    // Set physics
    this_world = world;
    customPhysics.pOwnerTransform = &transform;
    customPhysics.bodyType = CustomPhysics::DYNAMIC;
    customPhysics.bodyShape = CustomPhysics::BOX;
    customPhysics.AllocateBody(world);
    customPhysics.GetBody()->SetGravityScale(0.0f);
}

void Potion::Update()
{
    // if the chick collide with this object
    // do action after checking the type
    if (potion_type == Kind_Of_Object::Timer)
    {
        ActivePotion(10.0f);
    }
    if (potion_type == Kind_Of_Object::ExtendBody)
    {
        ActivePotion(20.0f);
    }
}