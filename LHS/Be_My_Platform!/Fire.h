/******************************************************************************/
/*!
\file   Fire.h
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "Item.h"

class Fire : public Item
{
private:
    // object for checking the collision with the chick
    CustomBaseObject fire_ghost;
public:
    void Initialize(b2Vec3 position, b2Vec2 scale, b2World* world, Chick * chick_pointer, Dog_Control *dog_control_pointer) override;
    void Update() override;

    // use for add and remove object
    CustomBaseObject* GetFireGhostAddress();
};