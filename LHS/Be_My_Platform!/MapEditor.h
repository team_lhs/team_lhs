/******************************************************************************/
/*!
\file   MapEditor.h
\par    Project Name : Be_My_Platform!
\author Primary author: SeongWook Shin, Secondary author: Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include <engine/State.h>
#include "CustomBaseObject.h"
#include "rapidjson/document.h"
#include "Door.h"

class MapEditor : public State
{
    friend class Game;

protected:

    MapEditor() : State() {};
    ~MapEditor() override {};

    // Derived initialize function
    void Initialize() override;
    // Derived Update function
    void Update(float dt) override;
    // Derived Close function
    void Close() override;

    void Set_CustomObject_Vector(void);
    void Make_New_Object(Kind_Of_Object kind);
    void Change_Current_Object(Kind_Of_Object number);
private:
    int Current_Map_Number = 0;

    int Total_Obejct_Number = -1;

    bool DogExist = false;
    bool ChickExist = false;

    // Objects
    std::vector<CustomBaseObject*>CustomObject;

    CustomBaseObject* Editing_Object;
};
