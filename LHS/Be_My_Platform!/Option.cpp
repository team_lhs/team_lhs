/******************************************************************************/
/*!
\file   Option.cpp
\par    Project Name : Be_My_Platform!
\author MinUi Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Option.h"
#include "engine/State.h" // State::m_renderer

void Option::Option_Init(b2World * world)
{
    World = world;

    BuildBox(BackArrow, "resume", 0.f, 135.f, 240.f, 60.f);
    BuildBox(Box_Sound, "sound", 0.f, 50.f, 240.f, 60.f);
    BuildBox(Box_Level, "levelselect", 0.f, -35.f, 240.f, 60.f);
    BuildBox(Box_Howtoplay, "howtoplay", 0.f, -120.f, 240.f, 60.f);
    BuildBox(Box_Exit, "exit", 0.f, -205.f, 240.f, 60.f);

    BuildBox(Box, "box", 0.f, 0.f, 350.f, 600.f);
}

void Option::Update(int sound)
{
    if (IsActive)
    {
        Box_Sound.sprite.color.a = 255;
        Box_Level.sprite.color.a = 255;
        Box_Howtoplay.sprite.color.a = 255;
        Box_Exit.sprite.color.a = 255;
        BackArrow.sprite.color.a = 255;

        Box.sprite.color.a = 255;

        if (!sound)
        {
            Box_Sound.sprite.Free();
            Box_Sound.sprite.LoadImage("texture/Option/soundoff.png", State::m_renderer);
        }
        else
        {
            Box_Sound.sprite.Free();
            Box_Sound.sprite.LoadImage("texture/Option/soundon.png", State::m_renderer);
        }
    }
    else
    {
        Box_Level.sprite.color.a = 0;
        Box.sprite.color.a = 0;
        Box_Howtoplay.sprite.color.a = 0;
        Box_Exit.sprite.color.a = 0;
        Box_Sound.sprite.color.a = 0;
        BackArrow.sprite.color.a = 0;
    }
}

CustomBaseObject & Option::BuildBox(CustomBaseObject & box, std::string name, float positionX, float positionY, float size_x, float size_y)
{
    box.SetName(name.c_str());
    box.transform.position.Set(positionX, positionY, 120.f);
    box.transform.SetScale(size_x, size_y);

    box.sprite.color = { 255, 255, 255, 0 };

    if (name == "sound")
        box.sprite.LoadImage("texture/Option/soundon.png", State::m_renderer);
    else if (name == "levelselect")
        box.sprite.LoadImage("texture/Option/levelselect.png", State::m_renderer);
    else if (name == "exit")
        box.sprite.LoadImage("texture/Option/exit.png", State::m_renderer);
    else if (name == "howtoplay")
        box.sprite.LoadImage("texture/Option/howtoplay.png", State::m_renderer);
    else if (name == "resume")
        box.sprite.LoadImage("texture/Option/resume.png", State::m_renderer);
    else if (name == "box")
    {
        box.sprite.LoadImage("texture/Option/OptionMenu.png", State::m_renderer);
        box.sprite.color = { 255,255,255,255 };
    }
    return box;
}
