/******************************************************************************/
/*!
\file   Credit.cpp
\par    Project Name : Be_My_Platform!
\author Minui Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "Credit.h"

void Credit::Initialize()
{
    SDL_ShowCursor(false);
    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0);
    m_useBuiltInPhysics = false;
    b2Vec2 gravity(0.f, 0.0f);
    SetCustomPhysicsWorld(gravity);
    mainFont = TTF_OpenFont("font/Default.ttf", 48);

    credit.transform.position.Set(0.f, -1200.f, 0.f);
    credit.transform.SetScale(650.f, 1700.f);
    credit.sprite.LoadImage("texture/PNG_FILE/Credit.png", m_renderer);
    credit.customPhysics.ActiveGhostCollision(true);

    backArrow.transform.position.Set(-500.f, 250.f, 0.0f);
    backArrow.transform.SetScale(70.f, 70.f);
    backArrow.sprite.LoadImage("texture/Button/Play.png", m_renderer);
    backArrow.transform.rotation = 180.f;
    backArrow.customPhysics.ActiveGhostCollision(true);

    background.transform.position.Set(0.f, -0.f, 0.f);
    background.transform.SetScale((float)Game::SCREEN_WIDTH, (float)Game::SCREEN_HEIGHT);
    background.sprite.LoadImage("texture/PNG_FILE/Credit_Background.png", m_renderer);
    background.customPhysics.ActiveGhostCollision(true);

    background.customPhysics.GenerateBody(GetCustomPhysicsWorld(), &background.transform);
    backArrow.customPhysics.GenerateBody(GetCustomPhysicsWorld(), &backArrow.transform);
    credit.customPhysics.GenerateBody(GetCustomPhysicsWorld(), &credit.transform);
    AddObject(&background);
    AddObject(&credit);
    AddObject(&backArrow);

    Mmouse.Mouse_Init(GetCustomPhysicsWorld());
    AddObject(&Mmouse);
}

void Credit::Update(float dt)
{
    float pos_y = credit.transform.position.y+1.f;
    credit.transform.position.Set(0.f, pos_y, 0.f);
    Mmouse.Mouse_Update(GetCustomPhysicsWorld());

    if (Mmouse.IsCollidingWith(&backArrow))
    {
        if (m_input->IsTriggered(SDL_BUTTON_LEFT))
            m_game->Change(State_Option);
    }

    //if (m_input->IsTriggered(SDL_SCANCODE_PAGEUP))
    //{
    //    m_game->Change(State_Option);
    //}

    Render(dt);
}

void Credit::Close()
{
    ClearBaseState();
}