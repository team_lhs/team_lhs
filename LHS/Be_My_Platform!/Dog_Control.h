/******************************************************************************/
/*!
\file   Dog_Control.h
\par    Project Name : Be_My_Platform!
\author SeongWook Shin

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "Dog.h"

class Dog_Control
{
public:
    Dog_Control(SDL_Renderer* m_renderer, b2World* physics_world,float x, float y, float scale = 64.0f, int dogver = 0);
    
    void Update_Dog(SDL_Renderer* m_renderer, float dt);
    bool Update_Head(SDL_Renderer* m_renderer, float dt);
    bool Update_Tail(SDL_Renderer* m_renderer, float dt);

    void Rotate_Dog(SDL_Renderer* m_renderer,b2Vec2 rotate_direction);
    void Extend_Dog(b2World* physics_world);

	void Set_Extend_Length(int Long);

    bool Make_Head(SDL_Renderer* m_renderer);
    
    void Erase_Dog_Tail(void);

    void False_Make_New_Head();
    void False_Make_New_Tail();

    std::vector<Dog*> Get_Dog_Address() const;
    Dog* Get_New_Head_Address() const;
    Dog* Get_New_Body_Address() const;
    bool Get_Make_New_Head() const;
    bool Get_Make_New_Tail() const;
    b2Vec3 Get_Head_Heading();

    void Close_Dog();

    void Die(bool dead);

    void Make_Dog_Alpha_to_0();

    CustomBaseObject Dog_Head_Ghost;

private:
    b2World * Current_world;

	int Extend_Length=0;
    float Dog_Scale;
    std::vector<Dog*>dog;
    b2Vec2 Head_Heading;

    bool Make_New_Head = false;
    bool Make_New_Tail = false;

    bool IsDead = false;

    int dog_ver;
};
