/******************************************************************************/
/*!
\file   resource.h
\par    Project Name : Be_My_Platform!
\author Primary author: SeongWook Shin, Secondary author: Jina Hyun

All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#define IDI_ICON1                       101

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
