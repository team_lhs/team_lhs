/******************************************************************************/
/*!
\file   WinScreen.h
\par    Project Name : Be_My_Platform!
\author Minui Lee, Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#pragma once
#include "engine\State.h"
#include "engine\Object.h"

class WinScreen : public State
{
    friend class Game;

protected:

    WinScreen() : State() {};
    ~WinScreen() override {};

    void Initialize() override;
    void Update(float dt) override;
    void Close() override;

    // to set the value of objects and add them
    void Set_Object(Object& object, const char* name, b2Vec3 position, b2Vec2 scale);
private:
    // the number that the player earn in the stage
    int Star_Number=0;

    const int period = 180;
    float ellapsedtime = 0.0f;

    // Background object
    Object Win_Screen;
    // buttons and stars object
    Object button[3], star[3];

    Mouse mouse;
};