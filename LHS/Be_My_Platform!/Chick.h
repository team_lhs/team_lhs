/******************************************************************************/
/*!
\file   Chick.h
\par    Project Name : Be_My_Platform!
\author MinUi Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "CustomBaseObject.h"

enum keys
{
    KEY_W,
    KEY_A,
    KEY_S,
    KEY_D,
    KEY_W_RELEASED,
    KEY_CheckY
};

enum direction
{
    RIGHT,
    LEFT,
    JUMP
};

class Chick
{
private:
    CustomBaseObject chick;

    float velocity = 5.0f;
    float past_y = 0.f;

    float Past_y = 0.f;

    // to check if the check die or not
    bool IsDead = false;
    // to check this stage is cleared or not
    bool IsFinish = false;
    bool CanJump = false;
    bool soundONOFF= false;
public:
    void Chick_Init(b2Vec3 position, b2World* world);
    void Chick_update(keys key);

    int Move_Direction;

    CustomBaseObject* chick_address();

    void Die(bool dead);
    void Finish(bool finish);

    void SpeedUp(float scale);

    bool IsLose();
    bool IsFinished();
    void setAnimi(int direction);
    bool CheckOkay(direction d);
};