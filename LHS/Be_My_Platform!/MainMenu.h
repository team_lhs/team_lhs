/******************************************************************************/
/*!
\file   MainMenu.h
\par    Project Name : Be_My_Platform!
\author Minui Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "engine\State.h"
#include "engine\Object.h"
#include "CustomBaseObject.h"
#include "Mouse.h"

class MainMenu : public State
{
    friend class Game;

protected:
    MainMenu() : State() {};
    ~MainMenu() override {};

    void Initialize() override;
    void Update(float dt) override;
    void Close() override;

private:
    float ellapsedtime = 0.0f;
    int i = 1;
    CustomBaseObject titleText, Background, Characters;
    CustomBaseObject Exit, Option, Play;
    CustomBaseObject left, right;
    Mouse Mmouse;

    void switchObj(int direction, CustomBaseObject* C1, CustomBaseObject *C2, CustomBaseObject *C3);
};
