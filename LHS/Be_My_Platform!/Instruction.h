/******************************************************************************/
/*!
\file   Instruction.h
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "CustomBaseObject.h"

class Instruction : public CustomBaseObject
{
public:
    // Check instrcution is shown or not
    bool isAlive = false;
    void Initialize(int type_num);
};