/******************************************************************************/
/*!
\file   OptionScreen.h
\par    Project Name : Be_My_Platform!
\author Minui Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "engine\State.h"
#include "engine\Object.h"
#include "CustomBaseObject.h"
#include "Mouse.h"

class OptionScreen : public State
{
    friend class Game;

protected:
    OptionScreen() : State() {};
    ~OptionScreen() override {};

    void Initialize() override;
    void Update(float dt) override;
    void Close() override;

private:
    float ellapsedtime = 0.0f;
    Mouse Mmouse;
    CustomBaseObject boxSound, boxFullscreen, boxCredit, backArrow, boxCustom, background;
    CustomBaseObject& BuildAndRegisterBody(CustomBaseObject &body, std::string name, float positionX, float positionY, float size_x, float size_y);

    CustomBaseObject ver0, ver1, ver2, ver3, ver4;
    CustomBaseObject box, selected;

    bool IsCustomOn = false;
};