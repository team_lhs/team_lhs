/******************************************************************************/
/*!
\file   Timer.h
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include <string>	// SetTimer(float) : to_string function
#include "CustomBaseObject.h"

class Timer : public CustomBaseObject
{
private:
	TTF_Font * timer_font;
	std::string timer = "00:00";

	int time_remain = 0;
	float time_limit = 0;
	float time_increase = 0;
    float reset_time = 0.0f;
    
	void SetRemainTimeString();

public:
	// Initialize the timer
	void InitTimer(TTF_Font* font, float limit_time);
	// time_remain goes down and SetText again
	void SetTimer(float ellapsedtime);
	// Check if time is over
	bool IsTimeOver();
	// Increase/Decrease the time_remain
	void IncreaseTime(float time_to_add);
        // When Restart the level
        void ResetTimer(float ellapsedtime);
    // return remain time
    int GetRemainTime();
    // set the remain time
    void SetRemainTime(float time);
};
