/******************************************************************************/
/*!
\file   Game.h
\par    Project Name : Be_My_Platform!
\author Minui Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once

#include "MainMenu.h"
#include "LevelOne.h"
#include "LoseScreen.h"
#include "WinScreen.h"
#include "SplashScreen.h"
#include "MapEditor.h"
#include "OptionScreen.h"
#include "LevelSelect.h"
#include "Credit.h"

typedef enum
{
    LV_splash = 0,
    LV_main,
    LV_one,

    LV_map_editor,
    State_lose,
    State_win,
    State_pause,
    State_Option,
    State_Credit,

    Level_select,

    LV_NUM_LEVELS
}Level_ID;

class StateManager;

class Game
{
	friend class Application;

	StateManager *m_stateManager = nullptr;

public:

	Game();
	~Game();

	void	RegisterState(State* state);
	void	SetFirstState(State* state);
	State*	GetCurrentState();
        
        int     GetCurrentMapStar();
	void	Quit();
	void	Change(unsigned stateId);
        void	Change(unsigned stateId,Kind_Of_Death Kind_Death);
        void	Change(unsigned stateId,int map_number);

	void	Save();

	bool	Initialize();
	void	Update(float dt);
	void	Close();

	void	RegisterPauseState(State *state);
	void	Pause();
	void	Resume();
	void	Restart();
	bool	IsQuit();
        
private:
	
	// All the level(state) declarations are here
        SplashScreen    splash_screen;
	MainMenu	mainmenu;

        LevelOne        level_one;

        MapEditor       level_map_editor;
        
        LoseScreen      lose_screen;
        WinScreen       win_screen;
        OptionScreen    option;
        Credit          credit;

        LevelSelect     level_select;

        Object click_sound;
        Object backgroundmusic;
        bool levelchanged = false;
        unsigned int previousid = LV_splash;
public:
	
	static constexpr int SCREEN_WIDTH = 1280, SCREEN_HEIGHT = 720;
	int	m_width = 0, m_height = 0;

	static bool isFullScreen;
	std::vector<int> Level_Star;

        static int Volume;
        static int Dog_ver;

        bool Is_Control_Change = false;

        // check the number of current level
        int current_map = 0;
        // store the total number of level
        int total_map = 0;
        // store the stars which the player earns for win screen
        int current_star = 0;

	Game(const Game&) = delete;
	Game(Game&&) = delete;
	Game& operator=(const Game&) = delete;
	Game& operator=(Game&&) = delete;

};

SDL_Color& operator+=(SDL_Color& a, SDL_Color b);
SDL_Color& operator-=(SDL_Color& a, SDL_Color b);