/******************************************************************************/
/*!
\file   Star.cpp
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Star.h"
#include "engine/State.h"	// State::m_renderer

void Star::Initialize(b2Vec3 position, b2Vec2 scale, b2World* world, Chick* chick_pointer, Dog_Control* dog_control_pointer)
{
    // Set the pointers
    this_world = world;
    chick = chick_pointer;
    dog_control = dog_control_pointer;
    
    // Set name
    SetName("Star");

    // Set transform
    transform.position.Set(position.x, position.y, position.z);
    transform.SetScale(scale);
    transform.rotation = 1;

    // Set sprite
    sprite.LoadImage("texture/Button/Icon_Star.png", State::m_renderer);
    sprite.color = { 255, 216, 0, 255 };

    // Set physics component
    customPhysics.pOwnerTransform = &transform;
    customPhysics.bodyType = CustomPhysics::STATIC;
    customPhysics.bodyShape = CustomPhysics::BOX;
    customPhysics.ActiveGhostCollision(true);
    customPhysics.AllocateBody(world);
}

void Star::Update()
{
    // if the chick collide with this object
    remove = Kind_Of_Death::Object_Death;
}