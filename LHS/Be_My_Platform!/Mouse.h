/******************************************************************************/
/*!
\file   Mouse.h
\par    Project Name : Be_My_Platform!
\author MinUi Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "CustomBaseObject.h"

class Mouse : public CustomBaseObject
{
public:
    void Mouse_Init(b2World* world);
    void Mouse_Update(b2World*world);
    bool IsCollidingWith(CustomBaseObject *C2);
    bool IsCollidingWith(Object *C2);
};

