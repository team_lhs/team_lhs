/******************************************************************************/
/*!
\file   WinScreen.cpp
\par    Project Name : Be_My_Platform!
\author Minui Lee, Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "WinScreen.h"

const SDL_Color BLACK = { 0, 0, 0, 255 };

void WinScreen::Initialize()
{
    // store the star number in game
    Star_Number = m_game->current_star;
    
    // remove the standard cursor
    SDL_ShowCursor(false);

    // Set camera
    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0);
    
    // Set the gravity for mouse
    m_useBuiltInPhysics = false;
    b2Vec2 gravity(0.f, -9.8f);
    SetCustomPhysicsWorld(gravity);

    // Set the objects
    Set_Object(Win_Screen, "WinScreen", { 0.0f, 0.0f, 0.0f }, { State::m_width, State::m_height });
    Set_Object(button[0], "Button_LevelSelect", { -123.0f, -185.0f, 50.0f }, { 180.0f, 180.0f });
    Set_Object(button[1], "Button_Play", { 172.0f, -180.f, 50.0f }, { 210.0f, 210.0f });
    Set_Object(button[2], "Button_Next", { 463.0f, -185.0f, 50.0f }, { 180.0f, 180.0f });

    // Set the star object
    for (int i = 0; i < 3; i++)
        Set_Object(star[i], "Star", { 170.0f + 250.0f * (i - 1), 50.0f, 50.0f }, { 150.0f, 150.0f });

    // make star yellow according the star that the player earn in the stage
    for (int i = 0; i < Star_Number; i++)
        star[i].sprite.color = { 255, 216, 0, 255 };

    // mouse
    mouse.Mouse_Init(GetCustomPhysicsWorld());
    AddObject(&mouse);
}

void WinScreen::Update(float dt)
{
    // mouse update
    mouse.Mouse_Update(GetCustomPhysicsWorld());

    // check for collision
    for (int i = 0; i < 3; i++)
    {
        if (mouse.IsCollidingWith(&button[i]))
        {
            // change the color if the mouse hovered on button
            button[i].sprite.color.a = 50;

            if (m_input->IsTriggered(SDL_BUTTON_LEFT))
            {
                // first button is level-select button
                if (i == 0)
                    m_game->Change(Level_select);
                // second button is retry(play) button
                else if (i == 1)
                    m_game->Change(LV_one);
                // third button is next level button
                else if (i == 2)
                {
                    // if it is last stage go to level_select
                    if (m_game->current_map == m_game->total_map - 1)
                        m_game->Change(Level_select);
                    // if it isn't the last stage
                    else
                        m_game->Change(LV_one, m_game->current_map + 1);
                }
            }
        }
        else
            button[i].sprite.color.a = 0;
    }

    UpdatePhysics(dt);
    Render(dt);
}

void WinScreen::Close()
{
    // Wrap up state
    ClearBaseState();
}

void WinScreen::Set_Object(Object& object, const char* name, b2Vec3 position, b2Vec2 scale)
{
    // Set name
    object.SetName(name);

    // Set transform
    object.transform.position = position;
    object.transform.SetScale(scale);

    // Set default color (unvisible)
    object.sprite.color = { 0, 0, 0, 0 };

    // Background
    if (object.GetName() == "WinScreen")
    {
        object.sprite.LoadImage("texture/WinLose/Win.png", m_renderer);
        object.sprite.color = { 255, 255, 255, 255 };
    }
    // buttons
    else if (object.GetName() == "Button_LevelSelect" || 
                object.GetName() == "Button_Play"     ||
                object.GetName() == "Button_Next")
        object.sprite.LoadImage("texture/circle.png", m_renderer);
    // Stars
    else if (object.GetName() == "Star")
    {
        object.sprite.LoadImage("texture/Button/icon_star.png", m_renderer);
        object.sprite.color = { 222, 222, 222, 255 };
    }

    AddObject(&object);
}
