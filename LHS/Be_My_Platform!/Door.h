/******************************************************************************/
/*!
\file   Door.h
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "Item.h"

class Door : public Item
{
public:
    void Initialize(b2Vec3 position, b2Vec2 scale, b2World* world, Chick* chick_pointer, Dog_Control* dog_control_pointer) override;
    void Update() override;

    // if the chick collide with lever
    void Open_Door();
};