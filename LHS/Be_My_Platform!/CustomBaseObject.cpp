/******************************************************************************/
/*!
\file   CustomBaseObject.cpp
\par    Project Name : Be_My_Platform!
\author Primary author: SeongWook Shin, Secondary author: Minui Lee, Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CustomBaseObject.h"
#include "Box2D\Dynamics\b2World.h"
#include "engine/State.h"

CustomBaseObject::CustomBaseObject()
    :Object()	// Call base class' constructor
{}

CustomBaseObject::~CustomBaseObject()
{}


/**
* \brief
* Remove custom physics component
*
* \param world
* Address of custom box2d world
*/
void CustomBaseObject::RemoveCustomPhysicsComp(b2World* world,
    std::vector<CustomPhysics*>& cpv)
{
    // Check either if physics is allocated before
    if (customPhysics.m_body) {
        for (auto i = cpv.begin(); i != cpv.end(); ++i)
        {
            if ((*i)->GetBody() == this->customPhysics.GetBody())
            {
                cpv.erase(i);
                world->DestroyBody(customPhysics.m_body);
                customPhysics.m_body = nullptr;
                customPhysics.m_allocatedBody = false;
                break;
            }
        }
    }
}
/*
**
* \brief
* Remove custom physics component
* *New and polished way
*/
void CustomBaseObject::RemoveCustomPhysicsComponent()
{
    auto customPhysicsList = customPhysics.m_pList;
    b2World * customPhysicsWorld = customPhysics.m_world;
    for (auto cPhysics = customPhysicsList->begin();
        cPhysics != customPhysicsList->end(); ++cPhysics)
    {
        if (customPhysics.m_body == (*cPhysics)->m_body)
        {
            // Erase component from the list
            customPhysicsList->erase(cPhysics);
            // Deregister rigid body from the physics world
            customPhysicsWorld->DestroyBody(customPhysics.m_body);
            // Set pointer to null
            customPhysics.m_body = nullptr;
            customPhysics.m_allocatedBody = false;
            break;
        }
    }
}

void CustomBaseObject::Initialize(b2Vec3, b2Vec2, b2World*, Chick*, Dog_Control *)
{
    return;
}

void CustomBaseObject::Update()
{
    return;
}

void CustomBaseObject::Move_Left_Object(b2World* world)
{
    transform.position = transform.position - b2Vec3{ 0.5f,0,0 };
    customPhysics.AllocateBody(world);
}

void CustomBaseObject::Move_Right_Object(b2World * world)
{
    transform.position = transform.position + b2Vec3{ 0.5f,0,0 };
    customPhysics.AllocateBody(world);
}
