/******************************************************************************/
/*!
\file   OptionScreen.h
\par    Project Name : Be_My_Platform!
\author Minui Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "OptionScreen.h"

void OptionScreen::Initialize()
{
    SDL_ShowCursor(false);
    m_backgroundColor = { 255,255,255,255 };

    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0);

    m_useBuiltInPhysics = false;
    b2Vec2 gravity(0.f, 0.0f);
    SetCustomPhysicsWorld(gravity);

    SDL_ShowCursor(false);
    Mmouse.Mouse_Init(GetCustomPhysicsWorld());
    IsCustomOn = false;

    BuildAndRegisterBody(background, "background", 0.f, 0.f, (float)Game::SCREEN_WIDTH, (float)Game::SCREEN_HEIGHT);

    BuildAndRegisterBody(box, "box", 0.f, 0.f, 450.f, 700.f);

    BuildAndRegisterBody(backArrow, "arrow", 0.f, 160.f, 290.f, 90.f);
    BuildAndRegisterBody(boxSound, "sound", 0.f, 60.f, 290.f, 90.f);
    BuildAndRegisterBody(boxFullscreen, "screen", 0.f, -40.f, 290.f, 90.f);
    BuildAndRegisterBody(boxCredit, "credit", 0.f, -140.f, 290.f, 90.f);
    BuildAndRegisterBody(boxCustom, "custom", 0.f, -240.f, 290.f, 90.f);

    BuildAndRegisterBody(ver1, "ver1", 120.f, 50.f, 120.f, 120.f);
    BuildAndRegisterBody(ver2, "ver2", -120.f, 50.f, 120.f, 120.f);
    BuildAndRegisterBody(ver3, "ver3", 120.f, -210.f, 120.f, 120.f);
    BuildAndRegisterBody(ver4, "ver4", -120.f, -210.f, 120.f, 120.f);
    BuildAndRegisterBody(ver0, "ver0", 0.f, -90.f, 120.f, 120.f);
    BuildAndRegisterBody(selected, "selected", 0.f, -90.f, 120.f, 120.f);

    AddObject(&Mmouse);
}

void OptionScreen::Update(float dt)
{
    if (!IsCustomOn)
    {
        boxSound.sprite.color.a = 255;
        boxFullscreen.sprite.color.a = 255;
        boxCredit.sprite.color.a = 255;
        boxCustom.sprite.color.a = 255;
        backArrow.sprite.Free();
        backArrow.sprite.LoadImage("texture/Option/mainmenu.png", m_renderer);

        if (Game::isFullScreen)
        {
            boxFullscreen.sprite.Free();
            boxFullscreen.sprite.LoadImage("texture/Option/fullscreeno.png", m_renderer);
        }
        else
        {
            boxFullscreen.sprite.Free();
            boxFullscreen.sprite.LoadImage("texture/Option/fullscreenx.png", m_renderer);
        }

        if (m_input->IsTriggered(SDL_BUTTON_LEFT))
        {
            if (Mmouse.IsCollidingWith(&backArrow))
            {
                m_game->Change(LV_main);
            }
            if (Mmouse.IsCollidingWith(&boxFullscreen))
            {
                Game::isFullScreen = !Game::isFullScreen;
            }
            if (Mmouse.IsCollidingWith(&boxCredit))
            {
                m_game->Change(State_Credit);
            }
            if (Mmouse.IsCollidingWith(&boxSound))
            {
                if (!Game::Volume)
                    Game::Volume = 128;
                else
                    Game::Volume = 0;
            }
            if (Mmouse.IsCollidingWith(&boxCustom))
            {
                IsCustomOn = !IsCustomOn;
            }
        }

        //if (m_input->IsTriggered(SDL_SCANCODE_PAGEUP))
        //{
        //    m_game->Change(LV_main);
        //}
        //if (m_input->IsTriggered(SDL_SCANCODE_PAGEDOWN))
        //{
        //    m_game->Change(State_Credit);
        //}

        if (!Game::Volume)
        {
            boxSound.sprite.Free();
            boxSound.sprite.LoadImage("texture/Option/soundoff.png", m_renderer);
        }
        else
        {
            boxSound.sprite.Free();
            boxSound.sprite.LoadImage("texture/Option/soundon.png", m_renderer);
        }

        ver0.sprite.color.a = 0;
        ver1.sprite.color.a = 0;
        ver2.sprite.color.a = 0;
        ver3.sprite.color.a = 0;
        ver4.sprite.color.a = 0;
        selected.sprite.color.a = 0;
    }
    else
    {
        backArrow.sprite.Free();
        backArrow.sprite.LoadImage("texture/Option/option.png", m_renderer);
        boxSound.sprite.color.a = 0;
        boxFullscreen.sprite.color.a = 0;
        boxCredit.sprite.color.a = 0;
        boxCustom.sprite.color.a = 0;

        ver0.sprite.color.a = 255;
        ver1.sprite.color.a = 255;
        ver2.sprite.color.a = 255;
        ver3.sprite.color.a = 255;
        ver4.sprite.color.a = 255;
        selected.sprite.color.a = 255;

        if (m_input->IsTriggered(SDL_BUTTON_LEFT))
        {
            if (Mmouse.IsCollidingWith(&backArrow))
            {
                IsCustomOn = false;
            }
            if (Mmouse.IsCollidingWith(&ver0))
            {
                Game::Dog_ver = 0;
                selected.transform.position = ver0.transform.position;
            }
            if (Mmouse.IsCollidingWith(&ver1))
            {
                Game::Dog_ver = 1;
                selected.transform.position = ver1.transform.position;
            }
            if (Mmouse.IsCollidingWith(&ver2))
            {
                Game::Dog_ver = 2;
                selected.transform.position = ver2.transform.position;
            }
            if (Mmouse.IsCollidingWith(&ver3))
            {
                Game::Dog_ver = 3;
                selected.transform.position = ver3.transform.position;
            }
            if (Mmouse.IsCollidingWith(&ver4))
            {
                selected.transform.position = ver4.transform.position;
                Game::Dog_ver = 4;
            }
        }
    }

    Mmouse.Mouse_Update(GetCustomPhysicsWorld());

    Render(dt);
}

void OptionScreen::Close()
{
    ClearBaseState();
}

CustomBaseObject& OptionScreen::BuildAndRegisterBody(CustomBaseObject &body, std::string name, float positionX, float positionY, float size_x, float size_y)
{
    body.SetName(name.c_str());
    body.transform.position.Set(positionX, positionY, 0.f);
    body.transform.SetScale(size_x, size_y);

    body.sprite.color = { 255, 255, 255, 255 };

    if (name == "sound")
        body.sprite.LoadImage("texture/Option/soundon.png", m_renderer);
    else if (name == "screen")
        body.sprite.LoadImage("texture/Option/fullscreenx.png", m_renderer);
    else if (name == "credit")
        body.sprite.LoadImage("texture/Option/credit.png", m_renderer);
    else if (name == "arrow")
        body.sprite.LoadImage("texture/Option/mainmenu.png", m_renderer);
    else if (name == "background")
        body.sprite.LoadImage("texture/PNG_FILE/Sky.png", m_renderer);
    else if (name == "custom")
        body.sprite.LoadImage("texture/Option/custom.png", m_renderer);
    else if (name == "ver0")
        body.sprite.LoadImage("texture/Character/Dog_Head_ver0.png", m_renderer);
    else if (name == "ver1")
        body.sprite.LoadImage("texture/Character/Dog_Head_ver1.png", m_renderer);
    else if (name == "ver2")
        body.sprite.LoadImage("texture/Character/Dog_Head_ver2.png", m_renderer);
    else if (name == "ver3")
        body.sprite.LoadImage("texture/Character/Dog_Head_ver3.png", m_renderer);
    else if (name == "ver4")
        body.sprite.LoadImage("texture/Character/Dog_Head_ver4.png", m_renderer);
    else if (name == "selected")
        body.sprite.LoadImage("texture/Option/selected.png", m_renderer);
    else if (name == "box")
        body.sprite.LoadImage("texture/Option/OptionMenu.png", m_renderer);

    body.customPhysics.bodyType = CustomPhysics::DYNAMIC;
    body.customPhysics.bodyShape = CustomPhysics::BOX;
    body.customPhysics.ActiveGhostCollision(true);
    body.customPhysics.GenerateBody(GetCustomPhysicsWorld(), &body.transform);

    AddObject(&body);
    AddCustomPhysicsComponent(&body);

    return body;
}
