/******************************************************************************/
/*!
\file   Goal.cpp
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Goal.h"
#include "engine/State.h"	// State::m_renderer

void Goal::Initialize(b2Vec3 position, b2Vec2 scale, b2World* world, Chick* chick_pointer, Dog_Control* dog_control_pointer)
{
    // Set pointer
    this_world = world;
    chick = chick_pointer;
    dog_control = dog_control_pointer;

    // Set name
    SetName("Goal");

    // Set transform
    transform.position.Set(position.x, position.y, position.z);
    transform.SetScale(scale.x, scale.y);
    transform.rotation = 0.0f;

    // Set sprite
    sprite.LoadImage("texture/Items/Door.png", State::m_renderer);

    // Set customphysics
    customPhysics.pOwnerTransform = &transform;
    customPhysics.bodyType = CustomPhysics::STATIC;
    customPhysics.bodyShape = CustomPhysics::BOX;
    customPhysics.ActiveGhostCollision(true);
    customPhysics.AllocateBody(world);
}

void Goal::Update()
{
    // if chick collide with this object
    // the player clear the stage
    chick->Finish(true);
}