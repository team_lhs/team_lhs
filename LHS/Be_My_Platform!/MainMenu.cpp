/******************************************************************************/
/*!
\file   MainMenu.cpp
\par    Project Name : Be_My_Platform!
\author Minui Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "MainMenu.h"

const SDL_Color BLACK = { 0, 0, 0, 255 };
const SDL_Color LIGHT_BLUE = { 230, 255, 255, 255 };
const SDL_Color GREY = { 150,200,125,255 };

void MainMenu::Initialize()
{
    SDL_ShowCursor(false);
    m_backgroundColor = LIGHT_BLUE;

    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0);

    m_useBuiltInPhysics = false;
    b2Vec2 gravity(0.f, 0.0f);
    SetCustomPhysicsWorld(gravity);

    i = 0;

    Background.transform.position.SetZero();
    Background.transform.SetScale((float)Game::SCREEN_WIDTH, (float)Game::SCREEN_HEIGHT);
    Background.sprite.LoadImage("texture/PNG_FILE/MainMenu_Background.png", State::m_renderer);

    titleText.transform.position.Set(0.f, 250.f, 0.0f);
    titleText.transform.SetScale(900.f, 162.f);
    titleText.sprite.LoadImage("texture/Logo/Title.png", State::m_renderer);

    Characters.transform.position.Set(-100.0f, -180.0f, 0.0f);
    Characters.transform.SetScale(300.0f, 225.f);
    Characters.sprite.LoadImage("texture/Character/MenuMenu_Characters.png", State::m_renderer);

    Option.sprite.LoadImage("texture/Button/Option.png", State::m_renderer);
    Option.sprite.color.a = 125;
    Option.customPhysics.bodyShape = CustomPhysics::CIRCLE;
    Option.transform.SetScale(200.f, 200.f);
    Option.transform.position.Set(300.f, 0.f, 0.f);
    Option.customPhysics.radius = 100.f;
    Option.customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Option.transform);

    Exit.sprite.LoadImage("texture/Button/Exit.png", State::m_renderer);
    Exit.sprite.color.a = 125;
    Exit.customPhysics.bodyShape = CustomPhysics::CIRCLE;
    Exit.transform.position.Set(-300.f, 0.f, 0.f);
    Exit.transform.SetScale(200.f, 200.f);
    Exit.customPhysics.radius = 100.f;
    Exit.customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Exit.transform);

    Play.transform.position.Set(0.f, 0.f, 0.f);
    Play.sprite.LoadImage("texture/Button/Play_i.png", State::m_renderer);
    Play.sprite.color.a = 255;
    Play.customPhysics.bodyShape = CustomPhysics::CIRCLE;
    Play.transform.SetScale(350.f, 350.f);
    Play.customPhysics.radius = 175.f;
    Play.customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Play.transform);

    left.transform.position.Set(-500.f, 0.f, 0.f);
    left.transform.SetScale(100.f, 100.f);
    left.sprite.color.a = 255;
    left.sprite.LoadImage("texture/Button/Play.png", State::m_renderer);
    left.transform.rotation = 180.f;

    right.transform.position.Set(500.f, 0.f, 0.f);
    right.transform.SetScale(100.f, 100.f);
    right.sprite.color.a = 255;
    right.sprite.LoadImage("texture/Button/Play.png", State::m_renderer);

    Mmouse.Mouse_Init(GetCustomPhysicsWorld());

    AddObject(&Background);

    AddObject(&Option);
    AddObject(&Exit);
    AddObject(&Play);
    AddObject(&titleText);
    AddObject(&left);
    AddObject(&right);

    AddObject(&Mmouse);

    AddObject(&Characters);
}

void MainMenu::Update(float dt)
{
    ellapsedtime += dt;
    Characters.transform.position.Set(-750.f + ellapsedtime * 100.f, -155.0f, -50.f);

    if (Characters.transform.position.x >= 800.0f)
    {
        Characters.transform.position.Set(-750.f, -155.0f, 0.0f);
        ellapsedtime = 0.0f;
    }

    if (m_input->IsTriggered(SDL_BUTTON_LEFT) && Mmouse.IsCollidingWith(&right))
    {
        std::cout << i << std::endl;
        if (i == 2)
        {
            switchObj(1, &Play, &Option, &Exit);
            i--;
        }
        else if (i == 1)
        {
            switchObj(1, &Option, &Exit, &Play);
            i--;
        }
        else if (i == 0)
        {
            i = 2;
            switchObj(1, &Exit, &Play, &Option);
        }
    }
    if (m_input->IsTriggered(SDL_BUTTON_LEFT) && Mmouse.IsCollidingWith(&left))
    {
        if (i == 0)
        {
            switchObj(0, &Exit, &Play, &Option);
            i++;
        }
        else if (i == 1)
        {
            switchObj(0, &Option, &Exit, &Play);
            i++;
        }
        else if (i == 2)
        {
            i = 0;
            switchObj(0, &Play, &Option, &Exit);
        }
    }

    if (m_input->IsTriggered(SDL_BUTTON_LEFT) && Mmouse.IsCollidingWith(&Play) && Play.transform.GetScale().x == 350.f)
        m_game->Change(Level_select);
    if (m_input->IsTriggered(SDL_BUTTON_LEFT) && Mmouse.IsCollidingWith(&Exit) && Exit.transform.GetScale().x == 350.f)
        m_game->Quit();
    if (m_input->IsTriggered(SDL_BUTTON_LEFT) && Mmouse.IsCollidingWith(&Option) && Option.transform.GetScale().x == 350.f)
        m_game->Change(State_Option);
    //if (m_input->IsTriggered(SDL_SCANCODE_PAGEUP))
    //    m_game->Change(Level_select);

    Mmouse.Mouse_Update(GetCustomPhysicsWorld());

    Render(dt);
}

void MainMenu::Close()
{
    ClearBaseState();
}

void MainMenu::switchObj(int direction, CustomBaseObject *C1, CustomBaseObject *C2, CustomBaseObject *C3) //0 == right  1 == left
{
    if (direction == 0)
    {
        C2->transform.position.Set(300.f, 0.f, 0.f);
        C2->transform.SetScale(200.f, 200.f);
        C2->sprite.color.a = 125;

        C3->transform.position.Set(-300.f, 0.f, 0.f);
        C3->transform.SetScale(200.f, 200.f);
        C3->sprite.color.a = 125;

        C1->transform.position.Set(0.f, 0.f, 0.f);
        C1->transform.SetScale(350.f, 350.f);
        C1->sprite.color.a = 255;
        C1->customPhysics.radius = 175.f;

    }
    else if (direction == 1)
    {
        C2->transform.position.Set(-300.f, 0.f, 0.f);
        C2->transform.SetScale(200.f, 200.f);
        C2->sprite.color.a = 125;
        C3->customPhysics.radius = 100.f;

        C1->transform.position.Set(300.f, 0.f, 0.f);
        C1->transform.SetScale(200.f, 200.f);
        C1->sprite.color.a = 125;
        C3->customPhysics.radius = 100.f;

        C3->transform.position.Set(0.f, 0.f, 0.f);
        C3->transform.SetScale(350.f, 350.f);
        C3->sprite.color.a = 255;
        C3->customPhysics.radius = 175.f;

    }
}