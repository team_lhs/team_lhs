/******************************************************************************/
/*!
\file   Door.cpp
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Door.h"
#include "engine/State.h"	// State::m_renderer

void Door::Initialize(b2Vec3 position, b2Vec2 scale, b2World* world, Chick* chick_pointer, Dog_Control* dog_control_pointer)
{
    // Set pointers
    this_world = world;
    chick = chick_pointer;
    dog_control = dog_control_pointer;

    // Set name
    SetName("Door");

    // Set transform
    transform.position.Set(position.x, position.y, position.z);
    transform.SetScale(scale.x, scale.y);
    transform.rotation = 0.0f;

    // Set spirte
    sprite.LoadImage("texture/Items/Thunder_Cloud.png", State::m_renderer);

    // Set customphysics
    customPhysics.pOwnerTransform = &transform;
    customPhysics.bodyType = CustomPhysics::STATIC;
    customPhysics.bodyShape = CustomPhysics::BOX;
    customPhysics.ActiveGhostCollision(false);
    customPhysics.AllocateBody(world);
}

void Door::Update()
{
}

void Door::Open_Door()
{
    sprite.Free();
    sprite.LoadImage("texture/Items/Clean_Cloud.png", State::m_renderer);
    customPhysics.ActiveGhostCollision(true);
    customPhysics.AllocateBody(this_world);
}
