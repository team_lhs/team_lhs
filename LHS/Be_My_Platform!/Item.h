/******************************************************************************/
/*!
\file   Item.h
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "CustomBaseObject.h"
#include "Chick.h"
#include "Dog_Control.h"

// All interaction objects inherit this class
class Item : public CustomBaseObject
{
public:
    // to allocate the object
    b2World * this_world = nullptr;
    // to send the die or finish message
    Chick *chick = nullptr;
    Dog_Control *dog_control = nullptr;
};