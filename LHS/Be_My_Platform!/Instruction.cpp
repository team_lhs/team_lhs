/******************************************************************************/
/*!
\file   Instruction.cpp
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Instruction.h"
#include <engine/State.h>   // sprite.LoadImage

void Instruction::Initialize(int type_num)
{
    // Set name
    SetName("Instruction");

    // Set transform
    transform.position.Set(0.0f, 0.0f, 0.0f);
    transform.SetScale(800.0f, 600.0f);
    transform.rotation = 0.0f;
    sprite.Free();
    // Set sprite
    sprite.activeAnimation = false;
    sprite.stopAnimation = true;
    sprite.SetFrame(1);

    // Set the image
    if (type_num == 0)
    {
        // when the player press "HOW TO PLAY" in option
        sprite.LoadImage("texture/Instruction/Instruction_animation.png", State::m_renderer);
        sprite.SetFrame(5);
        sprite.SetSpeed(1.0f);
        transform.SetScale({ 800.f,600.f });
        sprite.activeAnimation = true;
        sprite.stopAnimation=false;
        sprite.color.a = 0;
    }
    else if (type_num == 1)
        sprite.LoadImage("texture/Instruction/Instruction_1.png", State::m_renderer);
    else if (type_num == 2)
        sprite.LoadImage("texture/Instruction/Instruction_2.png", State::m_renderer);
    else if (type_num == 3)
        sprite.LoadImage("texture/Instruction/Instruction_3.png", State::m_renderer);
    else if (type_num == 4)
        sprite.LoadImage("texture/Instruction/Instruction_4.png", State::m_renderer);
    else if (type_num == 5)
        sprite.LoadImage("texture/Instruction/Instruction_5.png", State::m_renderer);
    sprite.isHud = true;

    isAlive = true;
}