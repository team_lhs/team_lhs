/******************************************************************************/
/*!
\file   CustomBaseObject.h
\par    Project Name : Be_My_Platform!
\author Primary author: SeongWook Shin, Secondary author: Minui Lee, Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "engine/Object.h"
#include "CustomPhysics.h"
#include <vector>

class Chick;
class Dog_Control;

// For check how the player die
enum class Kind_Of_Death : unsigned int
{
    alive=0,
    Object_Death=1,
    Fire_Chicken=2,
    Eating_Chicken=3,
    Timed_Death=4
};

// All objects in the game
enum class Kind_Of_Object
{
    Wall = 1,
    Chick,
    Dog,
    ExtendBody, // Potion object
    Goal,
    Fire,
    Lever,
    Door,
    Star,
    Timer,      // Potion object
    FixedCamera,// no-move map
    Ground,
    Number_Of_Object // total number of object in game
};

class CustomPhysics;

class CustomBaseObject : public Object
{

public:
    Kind_Of_Death remove = Kind_Of_Death::alive;
    int kind = static_cast<int>(Kind_Of_Object::Wall);
    // Is the object is moving object
    int Is_Moving = 0;

    CustomBaseObject();
    ~CustomBaseObject() override;

    // This is not built-in remove function
    // So you have to call this function 
    // whenever you delete an object contains custom physics
    void RemoveCustomPhysicsComp(b2World* world,
        std::vector<CustomPhysics*> &cpv);

    // New way to remove custom physics
    void RemoveCustomPhysicsComponent();

    CustomPhysics customPhysics;

    virtual void Initialize(b2Vec3 position, b2Vec2 scale, b2World *world, Chick * chick_pointer, Dog_Control *dog_control_pointer);
    virtual void Update();

    // For moving object
    void Move_Left_Object(b2World* world);
    void Move_Right_Object(b2World* world);
};