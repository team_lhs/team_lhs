/******************************************************************************/
/*!
\file   LevelOne.cpp
\par    Project Name : Be_My_Platform!
\author Primary Author: SeongWook Shin, Secondary Author: Minui Lee, Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "LevelOne.h"

#include "rapidjson/document.h"
#include "rapidjson/filewritestream.h"
#include "rapidjson/filereadstream.h"
#include <cstdio>
#include "Potion.h"
#include "Goal.h"
#include "Fire.h"
#include "Lever.h"
#include "Star.h"

using namespace rapidjson;

const SDL_Color OPAQUE = { 126,158,112,255 };

void LevelOne::Initialize()
{
    // Set Camera
    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, -500);
    // Set Fixed Camera
    FixedCamera = false;

    // Set Background
    Set_Hud_Object(back, "background", { 0.0f, 0.0f, -500.0f }, { State::m_width, State::m_height });

    // remove standard cursor
    SDL_ShowCursor(false);

    // Set main font
    // font from here: http://software.naver.com/software/summary.nhn?softwareId=MFS_114510&categoryId=I0800000#
    mainFont = TTF_OpenFont("font/Scott___.ttf", 48);
    // Set the sound
    // when the chick stands on the dog
        //Set_Sound(chick_step_dog, "sound/dog/wav", 20);
    // when the player earn the star
    Set_Sound(star, "sound/star.wav");
    // when the player use item
    Set_Sound(item, "sound/item.wav");
    
    // Chick (Set the value for chick)
    chick.Die(false);
    chick.Finish(false);
    chick.chick_address()->remove = Kind_Of_Death::alive;

    // Set zero to star number
    starnum = 0;
    m_game->current_star = 0;

    // Set the gravity
    m_useBuiltInPhysics = false;
    b2Vec2 gravity(0.f, -9.8f);
    SetCustomPhysicsWorld(gravity);

    // Load all objects in json file
    Set_CustomObject_Vector();
    past_y = 0.f;

    // Set Option
    InitializeOption();

    // Set Instruction
    if (Current_Map_Number >5)
        instruction.Initialize(0);
    else if (Current_Map_Number == 0 || Current_Map_Number == 1)
        instruction.Initialize(1);
    else
        instruction.Initialize(Current_Map_Number);
    
    start_int = true;
    AddObject(&instruction);

    // Set Dog
    AddObject((dog_control->Get_Dog_Address()[0]));
    AddObject((dog_control->Get_Dog_Address()[1]));
    AddObject((dog_control->Get_Dog_Address()[2]));
    AddObject(&dog_control->Dog_Head_Ghost);
    AddObject(chick.chick_address());
    AddCustomPhysicsComponent((dog_control->Get_Dog_Address()[0]));
    AddCustomPhysicsComponent((dog_control->Get_Dog_Address()[1]));
    AddCustomPhysicsComponent((dog_control->Get_Dog_Address()[2]));
    AddCustomPhysicsComponent((&dog_control->Dog_Head_Ghost));
    AddCustomPhysicsComponent(chick.chick_address());

    // Set Chick
    chick.chick_address()->customPhysics.GetBody()->SetGravityScale(1.f);
    chick.chick_address()->transform.rotation = 0.f;
    chick.chick_address()->sprite.color = SDL_Color{ 255,255,255,255 };
    chick.chick_address()->customPhysics.ActiveCollision(true);

    // Set HUD objects
    Set_Hud_Object(Option_ico, "Button_Option", { m_width / 2.0f - 50.f, m_height / 2.0f - 50.f, 0.f }, { 60.0f, 60.0f });
    Set_Hud_Object(Pause_ico, "Button_Pause", { m_width / 2.0f - 120.f, m_height / 2.0f - 50.f, 0.f }, { 60.0f, 60.0f });
    Set_Hud_Object(Replay_ico, "Button_Retry", { m_width / 2.0f - 190.f, m_height / 2.0f - 50.f, 0.f }, { 60.0f, 60.0f });
    Set_Hud_Object(HUD, "HUD", { 0.0f, 0.0f, 0.0f }, { State::m_width, State::m_height });

    // Set mouse
    mouse_pos_y = 0.f;
    timer_for_mouse = 0.f;
    mouse.Mouse_Init(GetCustomPhysicsWorld());
    AddObject(&mouse);

    // Set the timer (font and time limit)
    if (Current_Map_Number == 5)
        timer.InitTimer(mainFont, 30.0f);
    else
        timer.InitTimer(mainFont, 180.0f);
    AddObject(&timer);
    AddCustomPhysicsComponent(&timer);

    // Set Status
    pause_pause = false;
    option.IsActive = false;

    IsPause = false;
    Is_Restart = false;
    Is_Chick_Dead = false;
    Is_Clear = false;
    Is_Timer_On = false;

    Dead_ellapsedtime = 0.0f;
    Clear_ellapsedtime = 0.0f;
}


void LevelOne::Update(float dt)
{
    // Set the background to follow the character(chick)
    back.transform.position = camera.position;

    //// CHEAT CODE
    //// if the player press 'X', the chick never die
    //if (m_input->IsTriggered(SDL_SCANCODE_X))
    //{
    //    No_Death = true;
    //}

    /**************** M O U S E **************/
    mouse.Mouse_Update(GetCustomPhysicsWorld());

    if (mouse_pos_y == mouse.transform.position.y)
        timer_for_mouse += dt;
    else
    {
        timer_for_mouse = 0.f;
        mouse.sprite.color.a = 255;
    }

    if (timer_for_mouse > 5.0f)
        mouse.sprite.color.a = 0;

    mouse_pos_y = mouse.transform.position.y;

    if (!IsPause)
        backgroundMusic.Resume();
    else
        backgroundMusic.Pause();

    /**************** I N S T R U C T I O N **************/
    if (start_int && (m_input->IsAnyTriggered() || m_input->IsTriggered(SDL_BUTTON_LEFT) || m_input->IsTriggered(SDL_BUTTON_RIGHT)))
    {
        start_int = false;
        instruction.isAlive = false;
        instruction.sprite.color.a = 0;
    }
    if (instruction.isAlive == true)
        instruction.sprite.color.a = 255;

    if (m_input->IsTriggered(SDL_BUTTON_LEFT))
    {
        if (mouse.IsCollidingWith(&Replay_ico) && !IsPause)
        {
            timer.ResetTimer(ellapsedtime);
            if (Current_Map_Number > 4)
                instruction.Initialize(0);
            else
                instruction.Initialize(Current_Map_Number + 1);
            start_int = true;
            m_game->Restart();
        }
    }

    if (!IsPause && !pause_pause && !start_int)
    {
        if (!FixedCamera)
            camera.position = { chick.chick_address()->transform.position.x, 0, -500 };

        /******************* I C O N ********************/
        if (m_input->IsTriggered(SDL_BUTTON_LEFT))
        {
            if (mouse.IsCollidingWith(&Option_ico))
            {
                IsPause = !IsPause;
                option.IsActive = !option.IsActive;
            }
            else if (mouse.IsCollidingWith(&Pause_ico))
            {
                Pause_ico.sprite.Free();
                Pause_ico.sprite.LoadImage("texture/Button/Play_i.png", m_renderer); // play
                pause_pause = !pause_pause;
            }
        }

        /******************* C H I C K ******************/
        chick.chick_address()->sprite.stopAnimation = false;

        if (!m_game->Is_Control_Change)
        {
            if (m_input->IsPressed(SDL_SCANCODE_W))
            {
                chick.Chick_update(KEY_W);
                Past_W_Pressed = true;
            }
            else if (Past_W_Pressed == true)
            {
                chick.Chick_update(KEY_W_RELEASED);
                Past_W_Pressed = false;
            }
            else
            {
                chick.Chick_update(KEY_CheckY);
            }

            if (m_input->IsPressed(SDL_SCANCODE_A) && !Is_Chick_Dead && !Is_Clear)
                chick.Chick_update(KEY_A);
            if (m_input->IsPressed(SDL_SCANCODE_D) && !Is_Chick_Dead && !Is_Clear)
                chick.Chick_update(KEY_D);
            if (m_input->IsTriggered(SDL_SCANCODE_A) && !Is_Chick_Dead && !Is_Clear)
                chick.setAnimi(-1);
            if (m_input->IsTriggered(SDL_SCANCODE_D) && !Is_Chick_Dead && !Is_Clear)
                chick.setAnimi(1);
            if (!m_input->IsPressed(SDL_SCANCODE_A) && !m_input->IsPressed(SDL_SCANCODE_D) && !Is_Chick_Dead && !Is_Clear)
                chick.setAnimi(0);
        }
        else
        {
            if (m_input->IsPressed(SDL_SCANCODE_UP))
            {
                chick.Chick_update(KEY_W);
                Past_W_Pressed = true;
            }
            else if (Past_W_Pressed == true)
            {
                chick.Chick_update(KEY_W_RELEASED);
                Past_W_Pressed = false;
            }
            else
            {
                chick.Chick_update(KEY_CheckY);
            }

            if (m_input->IsPressed(SDL_SCANCODE_LEFT) && !Is_Chick_Dead && !Is_Clear)
                chick.Chick_update(KEY_A);
            if (m_input->IsPressed(SDL_SCANCODE_RIGHT) && !Is_Chick_Dead && !Is_Clear)
                chick.Chick_update(KEY_D);
            if (m_input->IsTriggered(SDL_SCANCODE_LEFT) && !Is_Chick_Dead && !Is_Clear)
                chick.setAnimi(-1);
            if (m_input->IsTriggered(SDL_SCANCODE_RIGHT) && !Is_Chick_Dead && !Is_Clear)
                chick.setAnimi(1);
            if (!m_input->IsPressed(SDL_SCANCODE_LEFT) && !m_input->IsPressed(SDL_SCANCODE_RIGHT) && !Is_Chick_Dead && !Is_Clear)
                chick.setAnimi(0);
        }
        /************** T I M E R ***************/
        ellapsedtime += dt;

        if (count % 12 == 0)
        {
            timer.SetTimer(ellapsedtime);
            count = 0;
        }
        ++count;

        if (timer.GetRemainTime() <= 4.f&&Is_Timer_On == false)
        {
            Is_Timer_On = true;
            timer.sound.Free();
            timer.sound.LoadSound("sound/timer.wav");
            timer.sound.Play();
        }

        if (timer.GetRemainTime() <= 0)
        {
            timer.ResetTimer(ellapsedtime);
            m_game->Change(State_lose, Kind_Of_Death::Timed_Death);
            timer.SetRemainTime(90.0f);
        }

        /************************ D O G ************************/
        // Panning by pressing arrow keys
        if(chick.chick_address()->remove!=Kind_Of_Death::Fire_Chicken)
        {
            if (!m_game->Is_Control_Change)
            {
                if (m_input->IsPressed(SDL_SCANCODE_RIGHT))
                {
                    dog_control->Rotate_Dog(m_renderer, b2Vec2(1.f, 0.f));
                }
                if (m_input->IsPressed(SDL_SCANCODE_LEFT))
                {
                    dog_control->Rotate_Dog(m_renderer, b2Vec2(-1.f, 0.f));
                }
                if (m_input->IsPressed(SDL_SCANCODE_DOWN))
                {
                    dog_control->Rotate_Dog(m_renderer, b2Vec2(0.f, -1.f));
                }
                if (m_input->IsPressed(SDL_SCANCODE_UP))
                {
                    dog_control->Rotate_Dog(m_renderer, b2Vec2(0.f, 1.f));
                }
            }
            else
            {
                if (m_input->IsPressed(SDL_SCANCODE_D))
                {
                    dog_control->Rotate_Dog(m_renderer, b2Vec2(1.f, 0.f));
                }
                if (m_input->IsPressed(SDL_SCANCODE_A))
                {
                    dog_control->Rotate_Dog(m_renderer, b2Vec2(-1.f, 0.f));
                }
                if (m_input->IsPressed(SDL_SCANCODE_S))
                {
                    dog_control->Rotate_Dog(m_renderer, b2Vec2(0.f, -1.f));
                }
                if (m_input->IsPressed(SDL_SCANCODE_W))
                {
                    dog_control->Rotate_Dog(m_renderer, b2Vec2(0.f, 1.f));
                }
            }
        }
        ////Zooming by pressing arrow W and S key
        //if (m_input->IsPressed(SDL_SCANCODE_Z))
        //    dog_control->Extend_Dog(GetCustomPhysicsWorld());

        /******************* State ******************/
        //// Restart current level by pressing 'R' key
        //if (m_input->IsTriggered(SDL_SCANCODE_R))
        //{
        //    Is_Restart = true;
        //    instruction.sprite.Free();
        //    timer.ResetTimer(ellapsedtime);
        //    if (Current_Map_Number > 4)
        //        instruction.Initialize(0);
        //    else
        //        instruction.Initialize(Current_Map_Number + 1);
        //    start_int = true;
        //    m_game->Restart();
        //}
        //if (m_input->IsTriggered(SDL_SCANCODE_TAB))
        //{
        //    Is_Restart = true;
        //    timer.ResetTimer(ellapsedtime);           

        //    m_game->Change(Level_select);
        //}
        //// Move Next level by pressing 'Insert' key
        //if (m_input->IsTriggered(SDL_SCANCODE_INSERT))
        //{
        //    Is_Restart = true;
        //    timer.ResetTimer(ellapsedtime);

        //    m_game->Change(LV_map_editor);
        //}
        //// Move Level Select by pressing 'page_down' key
        //if (m_input->IsTriggered(SDL_SCANCODE_PAGEDOWN))
        //{
        //    Is_Restart = true;
        //    timer.ResetTimer(ellapsedtime);

        //    m_game->Change(Level_select);
        //}
        // WIN
        if (chick.IsFinished())
        {
            if (!Is_Clear)
            {
                chick.chick_address()->sound.Free();
                chick.chick_address()->sound.LoadSound("sound/Clear_Stage.wav");
                chick.chick_address()->sound.SetVolume(128);
                chick.chick_address()->sound.Play();
                Is_Clear = true;
            }
            else if (Is_Clear)
            {
                chick.chick_address()->customPhysics.GetBody()->SetGravityScale(0);
                chick.chick_address()->customPhysics.SetVelocity(0, 0);
                chick.chick_address()->sprite.Free();
                chick.chick_address()->sprite.activeAnimation = false;
                chick.chick_address()->sprite.LoadImage("texture/character/chick_good.png", m_renderer);
                chick.chick_address()->transform.SetScale(40.f, 60.f);
                Clear_ellapsedtime += dt;
            }
            if (Clear_ellapsedtime > 1.f)
            {
                Is_Restart = true;
                timer.ResetTimer(ellapsedtime);
                m_game->Change(State_win);
            }
        }

        //update dog. if tail is too short remove!
        dog_control->Update_Dog(m_renderer, dt);

        if (dog_control->Get_Make_New_Tail())
        {
            RemoveObject(dog_control->Get_Dog_Address()[0]);
            dog_control->Get_Dog_Address()[0]->RemoveCustomPhysicsComp(GetCustomPhysicsWorld(), m_customPhysicsList);
            dog_control->Erase_Dog_Tail();
            RemoveObject(dog_control->Get_Dog_Address()[0]);
            dog_control->Get_Dog_Address()[0]->RemoveCustomPhysicsComp(GetCustomPhysicsWorld(), m_customPhysicsList);
            dog_control->Erase_Dog_Tail();

            dog_control->False_Make_New_Tail();
        }
        if (dog_control->Get_Make_New_Head())
        {
            AddObject(dog_control->Get_New_Head_Address());
            AddObject(dog_control->Get_New_Body_Address());
            AddCustomPhysicsComponent(dog_control->Get_New_Head_Address());
            AddCustomPhysicsComponent(dog_control->Get_New_Body_Address());
            dog_control->False_Make_New_Head();
        }
        if (chick.chick_address()->customPhysics.IsColliding())
        {
            chick.chick_address()->customPhysics.ChickCollidingUpdate();
        }
        for (auto i = CustomObject.begin(); i != CustomObject.end();)
        {
            if ((*i)->Is_Moving&&!Is_Restart)
            {
                if((*i)->Is_Moving==1)
                   (*i)->Move_Left_Object(GetCustomPhysicsWorld());
                else if ((*i)->Is_Moving == 2)
                    (*i)->Move_Right_Object(GetCustomPhysicsWorld());
                if ((*i)->GetName() == "Fire")
                    dynamic_cast<Fire*>(*i)->Update();
            }
            if ((*i)->remove ==Kind_Of_Death:: Object_Death)
            {
                if ((*i)->GetName() == "Star")
                {
                    star.sound.Play();
                    starnum++;
                    m_game->current_star++;
                }
                else
                    item.sound.Play();

                RemoveObject((*i));
                (*i)->RemoveCustomPhysicsComp(GetCustomPhysicsWorld(), m_customPhysicsList);
                (*i)->customPhysics.Free();
                i = CustomObject.erase(i);
            }
            else
                i++;
        }

        if (chick.chick_address()->transform.position.y == past_y)
        {
            b2Vec2 temp_vel = chick.chick_address()->customPhysics.GetVelocity();
            chick.chick_address()->customPhysics.SetVelocity(temp_vel.x*0.8f, temp_vel.y);
        }
        else
        {
            b2Vec2 temp_vel = chick.chick_address()->customPhysics.GetVelocity();
            chick.chick_address()->customPhysics.SetVelocity(temp_vel.x*0.98f, temp_vel.y);
        }
        past_y = chick.chick_address()->customPhysics.pOwnerTransform->position.y;

        // Must be one of the last functions called at the end of State Update

        UpdateCustomPhysics(dt);

        if (!No_Death)
        {
            if (!Is_Chick_Dead&&chick.chick_address()->remove >= Kind_Of_Death::Fire_Chicken)
            {
                Is_Chick_Dead = true;
                if(chick.chick_address()->remove==Kind_Of_Death::Eating_Chicken)
                {
                    chick.chick_address()->sound.Free();
                    chick.chick_address()->sound.LoadSound("sound/Eating_Chicken.wav");
                    chick.chick_address()->sound.SetVolume(128);
                    chick.chick_address()->sound.Play();
                }
                else
                {
                    dog_control->Make_Dog_Alpha_to_0();
                    chick.chick_address()->sound.Free();
                    chick.chick_address()->sound.LoadSound("sound/Chicken_Fire.wav");
                    chick.chick_address()->sound.SetVolume(128);
                    chick.chick_address()->sound.Play();
                }
            }
            if(Is_Chick_Dead)
            {
                Dead_ellapsedtime += dt;
                chick.chick_address()->sprite.Free();
                chick.chick_address()->sprite.LoadImage("texture/character/chick.png",m_renderer);
                chick.chick_address()->sprite.activeAnimation=false;
                if (chick.chick_address()->remove == Kind_Of_Death::Fire_Chicken)
                {
                    chick.chick_address()->sprite.color -= SDL_Color{ 2,2,2,0 };
                    chick.chick_address()->transform.position = camera.position;
                    chick.chick_address()->transform.SetScale(chick.chick_address()->transform.GetScale() + b2Vec2{ 5.f,5.f });
                }
                else
                {
                    chick.chick_address()->customPhysics.ActiveCollision(false);
                    chick.chick_address()->transform.position = dog_control->Dog_Head_Ghost.transform.position+ dog_control->Get_Head_Heading();
                    chick.chick_address()->transform.SetScale(chick.chick_address()->transform.GetScale() - b2Vec2{ 0.5f,0.5f });
                }
                chick.chick_address()->transform.rotation += 12.f;
            }
            if(Dead_ellapsedtime>1.f)
            {
                m_game->Change(State_lose, chick.chick_address()->remove);
                timer.ResetTimer(ellapsedtime);
            }
        }
    }
    else if(IsPause || pause_pause)
    {
        if (chick.chick_address()->sprite.activeAnimation)
            chick.chick_address()->sprite.stopAnimation =true;

        if (!pause_pause)
        {
            if (m_input->IsTriggered(SDL_BUTTON_LEFT))
            {
                if (instruction.isAlive == false)
                {
                    if (mouse.IsCollidingWith(&option.BackArrow))
                    {
                        IsPause = !IsPause;
                        option.IsActive = !option.IsActive;
                        backgroundMusic.Resume();
                    }
                    if (mouse.IsCollidingWith(&option.Box_Exit))
                    {
                        m_game->Quit();
                    }
                    if (mouse.IsCollidingWith(&option.Box_Level))
                    {
                        std::cout << instruction.isAlive << std::endl;
                        timer.ResetTimer(ellapsedtime);
                        m_game->Change(Level_select);
                    }
                    if (mouse.IsCollidingWith(&option.Box_Sound))
                    {
                        if (!Game::Volume)
                            Game::Volume = 128;
                        else
                            Game::Volume = 0;
                    }
                    if (mouse.IsCollidingWith(&option.Box_Howtoplay))
                    {
                        instruction.Initialize(0);
                        instruction.sprite.color.a = 255;
                    }
                }
                else
                {
                    instruction.isAlive = false;
                    instruction.sprite.color.a = 0;
                }
            }
        }
        else
        {
            Pause_ico.sprite.Free();
            Pause_ico.sprite.LoadImage("texture/Button/Play_i.png", m_renderer);
            if (m_input->IsTriggered(SDL_BUTTON_LEFT))
            {
                if (mouse.IsCollidingWith(&Pause_ico))
                {
                    pause_pause = !pause_pause;
                    Pause_ico.sprite.Free();
                    Pause_ico.sprite.LoadImage("texture/Button/Pause.png", m_renderer);
                }
            }
        }
    }

    option.Update(Game::Volume);

    Render(dt);

}

void LevelOne::Close()
{
    if (chick.IsFinished())
    {
        if (starnum > m_game->Level_Star[Current_Map_Number])
        {
            m_game->Level_Star[Current_Map_Number] = starnum;
            m_game->Save();
        }
    }
    // Deallocate custom physics world
    RemoveCustomPhysicsWorld();

    //dog_control->Close_Dog();
    // Wrap up state
    ClearBaseState();
}


void LevelOne::Set_CustomObject_Vector(void)
{
    for (auto i = CustomObject.begin(); i != CustomObject.end();)
    {
        RemoveObject((*i));
        (*i)->RemoveCustomPhysicsComponent();
        (*i)->customPhysics.Free();
        delete *i;
        i = CustomObject.erase(i);
    }
    errno_t err;
    FILE* fp;
    std::string filename = std::to_string(Current_Map_Number);
    filename += ".json";
    err = fopen_s(&fp, filename.c_str(), "rb"); // non-Windows use "r"

    char readBuffer[65536];
    FileReadStream is(fp, readBuffer, sizeof(readBuffer));

    Document d;
    d.ParseStream(is);

    const Value& a = d["ObjectList"];
    assert(a.IsArray());
    for (SizeType i = 0; i < a.Size(); i++)
    {
        auto Ob = a[i].GetObject();

        if (Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::Wall) ||
            Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::Ground))
        {
            CustomBaseObject* temp_save = new CustomBaseObject;
            temp_save->Is_Moving = Ob.FindMember("Is_Moving")->value.GetInt();
            temp_save->SetName("wall");
            temp_save->transform.position.x =
                static_cast<float>(Ob.FindMember("x_pos")->value.GetDouble());
            temp_save->transform.position.y =
                static_cast<float>(Ob.FindMember("y_pos")->value.GetDouble());
            b2Vec2 Scale_Set;
            Scale_Set.x =
                static_cast<float>(Ob.FindMember("x_scale")->value.GetDouble());
            Scale_Set.y =
                static_cast<float>(Ob.FindMember("y_scale")->value.GetDouble());
            temp_save->transform.SetScale(Scale_Set);

            if (Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::Wall))
            {
                temp_save->sprite.color = OPAQUE;
                temp_save->sprite.color.a = 255;
                temp_save->sprite.LoadImage("texture/rect.png", m_renderer);
            }
            else if (Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::Ground))
            {
                temp_save->sprite.LoadImage("texture/ground.png", m_renderer);
            }

            temp_save->customPhysics.pOwnerTransform = &temp_save->transform;
            temp_save->customPhysics.bodyType = CustomPhysics::STATIC;
            temp_save->customPhysics.bodyShape = CustomPhysics::BOX;
            temp_save->customPhysics.AllocateBody(GetCustomPhysicsWorld(),_EntityCategory::Interaction_Object,_EntityCategory::Dog_Ghost|Interaction_Object);

            CustomObject.push_back(temp_save);
            AddObject(temp_save);
            AddCustomPhysicsComponent(temp_save);
        }
        else if (Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::Chick))
        {
            b2Vec3 temp_pos = b2Vec3(static_cast<float>(Ob.FindMember("x_pos")->value.GetDouble()),
                static_cast<float>(Ob.FindMember("y_pos")->value.GetDouble()), 0.f);
            chick.Chick_Init(temp_pos, GetCustomPhysicsWorld());
            chick.Move_Direction= Ob.FindMember("Is_Moving")->value.GetInt();
        }
        else if (Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::Dog))
        {
            dog_control = new Dog_Control(m_renderer, GetCustomPhysicsWorld(),
            static_cast<float>(Ob.FindMember("x_pos")->value.GetDouble()),
            static_cast<float>(Ob.FindMember("y_pos")->value.GetDouble()),
            static_cast<float>(Ob.FindMember("x_scale")->value.GetDouble()), Game::Dog_ver);
        }
        else if (Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::ExtendBody) ||
            Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::Timer))
        {
            Potion* temp_potion = new Potion;
            b2Vec3 temp_pos = b2Vec3(static_cast<float>(Ob.FindMember("x_pos")->value.GetDouble()),
                static_cast<float>(Ob.FindMember("y_pos")->value.GetDouble()), 0.f);
            b2Vec2 Scale_Set;
            Scale_Set.x =
                static_cast<float>(Ob.FindMember("x_scale")->value.GetDouble());
            Scale_Set.y =
                static_cast<float>(Ob.FindMember("y_scale")->value.GetDouble());
            temp_potion->Is_Moving = Ob.FindMember("Is_Moving")->value.GetInt();
            temp_potion->Initialize(temp_pos, Scale_Set, GetCustomPhysicsWorld(), &chick, dog_control);
            temp_potion->Set_PotionType((Kind_Of_Object)Ob.FindMember("Kind")->value.GetInt());
            if (Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::Timer))
                temp_potion->Set_TimerPointer(&timer);
            CustomObject.push_back(temp_potion);
            temp_potion->customPhysics.ActiveGhostCollision(true);
            AddObject(temp_potion);
            AddCustomPhysicsComponent(temp_potion);
        }
        else if (Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::Goal))
        {
            Goal* temp_goal = new Goal;
            b2Vec3 temp_pos = b2Vec3(static_cast<float>(Ob.FindMember("x_pos")->value.GetDouble()),
                static_cast<float>(Ob.FindMember("y_pos")->value.GetDouble()), 0.f); 
			b2Vec2 Scale_Set;
            Scale_Set.x =
                static_cast<float>(Ob.FindMember("x_scale")->value.GetDouble());
            Scale_Set.y =
                static_cast<float>(Ob.FindMember("y_scale")->value.GetDouble());
            temp_goal->Is_Moving = Ob.FindMember("Is_Moving")->value.GetInt();
            temp_goal->Initialize(temp_pos, Scale_Set, GetCustomPhysicsWorld(), &chick, dog_control);
			temp_goal->customPhysics.bodyType = CustomPhysics::BodyType::DYNAMIC;
			temp_goal->customPhysics.ActiveGhostCollision(true);
            CustomObject.push_back(temp_goal);
            AddObject(temp_goal);
            AddCustomPhysicsComponent(temp_goal);
        }
        else if (Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::Fire))
        {
            Fire* temp_fire = new Fire;
            b2Vec3 temp_pos = b2Vec3(static_cast<float>(Ob.FindMember("x_pos")->value.GetDouble()),
                static_cast<float>(Ob.FindMember("y_pos")->value.GetDouble()), 0.f); 
			b2Vec2 Scale_Set;
            Scale_Set.x =
                static_cast<float>(Ob.FindMember("x_scale")->value.GetDouble());
            Scale_Set.y =
                static_cast<float>(Ob.FindMember("y_scale")->value.GetDouble());
            temp_fire->Is_Moving = Ob.FindMember("Is_Moving")->value.GetInt();
            temp_fire->Initialize(temp_pos, Scale_Set, GetCustomPhysicsWorld(), &chick, dog_control);
            CustomObject.push_back(temp_fire);
            AddObject(temp_fire);
            AddObject(temp_fire->GetFireGhostAddress());
            AddCustomPhysicsComponent(temp_fire);
            AddCustomPhysicsComponent(temp_fire->GetFireGhostAddress());
        }
        else if (Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::Lever))
        {
            Lever* temp_lever = new Lever;
            b2Vec3 temp_pos = b2Vec3(static_cast<float>(Ob.FindMember("x_pos")->value.GetDouble()),
                static_cast<float>(Ob.FindMember("y_pos")->value.GetDouble()), 0.f); 
			b2Vec2 Scale_Set;
            Scale_Set.x =
                static_cast<float>(Ob.FindMember("x_scale")->value.GetDouble());
            Scale_Set.y =
                static_cast<float>(Ob.FindMember("y_scale")->value.GetDouble());
            temp_lever->Is_Moving = Ob.FindMember("Is_Moving")->value.GetInt();
            temp_lever->Initialize(temp_pos, Scale_Set, GetCustomPhysicsWorld(), &chick, dog_control);
			temp_lever->door = dynamic_cast<Door*>(*(CustomObject.end() - 1));
            CustomObject.push_back(temp_lever);
            AddObject(temp_lever);
            AddCustomPhysicsComponent(temp_lever);
        }
	else if (Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::Door))
	{
		Door* temp_door = new Door;
		b2Vec3 temp_pos = b2Vec3(static_cast<float>(Ob.FindMember("x_pos")->value.GetDouble()),
			static_cast<float>(Ob.FindMember("y_pos")->value.GetDouble()), 0.f);
		b2Vec2 Scale_Set;
		Scale_Set.x =
			static_cast<float>(Ob.FindMember("x_scale")->value.GetDouble());
		Scale_Set.y =
			static_cast<float>(Ob.FindMember("y_scale")->value.GetDouble());
                temp_door->Is_Moving = Ob.FindMember("Is_Moving")->value.GetInt();
		temp_door->Initialize(temp_pos, Scale_Set, GetCustomPhysicsWorld(), &chick, dog_control);
		CustomObject.push_back(temp_door);
		AddObject(temp_door);
		AddCustomPhysicsComponent(temp_door);
	}
        else if (Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::Star))
        {
            Star* temp_star = new Star;
            b2Vec3 temp_pos = b2Vec3(static_cast<float>(Ob.FindMember("x_pos")->value.GetDouble()),
                static_cast<float>(Ob.FindMember("y_pos")->value.GetDouble()), 0.f); 
			b2Vec2 Scale_Set;
            Scale_Set.x =
                static_cast<float>(Ob.FindMember("x_scale")->value.GetDouble());
            Scale_Set.y =
                static_cast<float>(Ob.FindMember("y_scale")->value.GetDouble());
            temp_star->Is_Moving = Ob.FindMember("Is_Moving")->value.GetInt();
            temp_star->Initialize(temp_pos, Scale_Set, GetCustomPhysicsWorld(), &chick, dog_control);
	    temp_star->customPhysics.ActiveGhostCollision(true);
            CustomObject.push_back(temp_star);
            AddObject(temp_star);
            AddCustomPhysicsComponent(temp_star);
        }
        else if (Ob.FindMember("Kind")->value.GetInt() == static_cast<int>(Kind_Of_Object::FixedCamera))
        {
            FixedCamera = true;
        }
    }
    fclose(fp);
}

void LevelOne::Set_Hud_Object(CustomBaseObject& object, const char* name, b2Vec3 position, b2Vec2 scale)
{
    object.SetName(name);

    object.transform.position = position;
    object.transform.SetScale(scale);

    object.sprite.isHud = true;

    if (object.GetName() == "Button_Option")
        object.sprite.LoadImage("texture/Button/Option_g.png", m_renderer);
    else if (object.GetName() == "Button_Pause")
        object.sprite.LoadImage("texture/Button/Pause_g.png", m_renderer);
    else if (object.GetName() == "Button_Retry")
        object.sprite.LoadImage("texture/Button/Retry_g.png", m_renderer);
    else if (object.GetName() == "HUD")
        object.sprite.LoadImage("texture/Boundary.png", m_renderer);
    else if(object.GetName() == "background")
    {
        object.sprite.LoadImage("texture/back.png", m_renderer);
        object.sprite.isHud = false;
    }

    AddObject(&object);
}

void LevelOne::Set_Sound(CustomBaseObject& object, const char* file_path, int volume)
{
    // Load sound from this file path
    object.sound.LoadSound(file_path);
    // Set the volume of sound
    object.sound.SetVolume(volume);
    AddObject(&object);
}

void LevelOne::InitializeOption()
{
    option.Option_Init(GetCustomPhysicsWorld());
    option.Box.sprite.isHud = true;
    option.Box_Howtoplay.sprite.isHud = true;
    option.Box_Exit.sprite.isHud = true;
    option.Box_Level.sprite.isHud = true;
    option.Box_Sound.sprite.isHud = true;
    option.BackArrow.sprite.isHud = true;

    AddObject(&option.Box);
    AddObject(&option.Box_Howtoplay);
    AddObject(&option.Box_Exit);
    AddObject(&option.Box_Level);
    AddObject(&option.Box_Sound);
    AddObject(&option.BackArrow);
}
