/******************************************************************************/
/*!
\file   LevelSelect.h
\par    Project Name : Be_My_Platform!
\author MinUi Lee, SeongWook Shin

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "engine\State.h"
#include "engine\Object.h"
#include "CustomBaseObject.h"
#include "Mouse.h"

class Box
{
public:
    void Init(int order);
    CustomBaseObject* boxAdd() { return &box; }
    CustomBaseObject* starOne() { return &star1; };
    CustomBaseObject* starTwo() { return &star2; };
    CustomBaseObject* starThree() { return &star3; };
    int Star_Number = 0;

private:
    int order_ = 0;
    CustomBaseObject star1;
    CustomBaseObject star2;
    CustomBaseObject star3;
    CustomBaseObject box;
};

class LevelSelect : public State
{
    friend class Game;

protected:
    LevelSelect() : State() {};
    ~LevelSelect() override {};

    void Initialize() override;
    void Update(float dt) override;
    void Close() override;

private:
    float ellapsedtime = 0.0f;
    int level_num;
    Mouse Mmouse;
    CustomBaseObject backArrow;
    CustomBaseObject background;
    std::vector<Box*> boxes;
    CustomBaseObject& BuildAndRegisterBody(CustomBaseObject &body, std::string name, float positionX, float positionY, float size_x, float size_y);
};
