/******************************************************************************/
/*!
\file   Option.h
\par    Project Name : Be_My_Platform!
\author MinUi Lee

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "CustomBaseObject.h"

class Option
{
public:
    void Option_Init(b2World* world);
    void Update(int sound);

    CustomBaseObject Box_Howtoplay, Box_Exit, Box_Sound, Box_Level, BackArrow, Box;
    bool IsActive = false;
private:
    CustomBaseObject& BuildBox(CustomBaseObject& box, std::string name, float positionX, float positionY, float size_x, float size_y);
    b2World* World;
};