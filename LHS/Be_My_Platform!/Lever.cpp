/******************************************************************************/
/*!
\file   Lever.cpp
\par    Project Name : Be_My_Platform!
\author Jina Hyun

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Lever.h"
#include "engine/State.h"	// State::m_renderer

void Lever::Initialize(b2Vec3 position, b2Vec2 scale, b2World* world, Chick* chick_pointer, Dog_Control* dog_control_pointer)
{
    // Set the pointers
    this_world = world;
    chick = chick_pointer;
    dog_control = dog_control_pointer;

    // Set name
    SetName("Switch");

    // Set transform
    transform.position.Set(position.x, position.y, position.z);
    transform.SetScale(scale.x, scale.y);
    transform.rotation = 0.0f;

    // Set spirte
    sprite.LoadImage("texture/Items/switchMid.png", State::m_renderer);

    // Set customphysics
    customPhysics.pOwnerTransform = &transform;
    customPhysics.bodyType = CustomPhysics::STATIC;
    customPhysics.bodyShape = CustomPhysics::BOX;
    customPhysics.AllocateBody(world);
    customPhysics.ActiveGhostCollision(true);
    Is_Sound_On = false;
}

void Lever::Update()
{
    if(!Is_Sound_On)
    {
        Is_Sound_On = true;
        sound.Free();
        sound.LoadSound("sound/Lever.wav");
        sound.Play();
    }
    // if the chick collide with this object
    sprite.Free();
    // change the sprite
    sprite.LoadImage("texture/Items/switchRight.png", State::m_renderer);
    // Open the door by using the Door pointer
    door->Open_Door();
}