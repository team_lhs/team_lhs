/******************************************************************************/
/*!
\file   Dog.h
\par    Project Name : Be_My_Platform!
\author SeongWook Shin

    All content c 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "CustomBaseObject.h"

enum Image_Direction
{
    Left,
    Right
};

class Dog:public CustomBaseObject
{
public:
    float Elapsed_Time = 0;
    float Next_Rotation;
    b2Vec2 Direction;
    bool Is_Animated;

    Image_Direction Rotate_Image_Direction = Left;
};
